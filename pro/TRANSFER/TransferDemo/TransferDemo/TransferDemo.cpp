// TransferDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <Windows.h>

#include "DldTool.h"


#ifdef _DEBUG
#pragma comment(lib,"transferdll.lib")
#else
#pragma comment(lib,"transferdll.lib")
#endif


enum{
    DEFAULT_CALIBRATE_VALUE = 127,
};

void MonitorDownloadEvent()
{
    while (1)
    {
        Sleep(10);
        unsigned long notifyValue =  get_notify_from_cext();
        bool bFinished = false;
        switch(notifyValue)
        {
        case ev_wm_port_open_failed        : 
            printf("/***************************************--------- ev_wm_port_open_failed      \n"); break;
        case ev_wm_port_open_succeed       :
            printf("/***************************************---------ev_wm_port_open_succeed     \n"); break;
        case ev_wm_sync_wait               :
            printf("/***************************************---------ev_wm_sync_wait             \n"); break;
        case ev_wm_sync_failed             :
            printf("/***************************************---------ev_wm_sync_failed           \n"); break;
        case ev_wm_sync_succeed            :
            printf("/***************************************---------ev_wm_sync_succeed          \n"); break;
        case ev_wm_run_programmer_failed   :
            printf("/***************************************---------ev_wm_run_programmer_failed \n"); break;
        case ev_wm_run_programmer_succeed  :
            printf("/***************************************---------ev_wm_run_programmer_succeed\n"); break;
        case ev_wm_bin_encrypt_begin       :
            printf("/***************************************---------ev_wm_bin_encrypt_begin     \n"); break;
        case ev_wm_bin_encrypt_end         :
            printf("/***************************************---------ev_wm_bin_encrypt_end       \n"); break;
        case ev_wm_update_sw_ver           : 
            printf("/***************************************---------ev_wm_update_sw_ver         \n"); break;
        case ev_wm_update_product_id       :
            printf("/***************************************---------ev_wm_update_product_id     \n"); break;
        case ev_wm_burn_progress           :
            printf("/***************************************---------ev_wm_burn_progress         \n"); 
            {
                int iProcess = get_burn_progress();
                //printf("/************************** process:%d********************\n", iProcess);
            }
            break;
        case ev_wm_burn_magic              : 
            printf("/***************************************---------ev_wm_burn_magic            \n"); break;
        case ev_wm_burn_failure            : 
            printf("/***************************************---------ev_wm_burn_failure          \n"); break;
        case ev_wm_burn_complt             :
            printf("/***************************************---------ev_wm_burn_complt           \n");
            bFinished = true;
            break;
        case ev_wm_burn_efuse_start        :
            printf("/***************************************---------ev_wm_burn_efuse_start      \n"); break;
        case ev_wm_burn_efuse_end          : 
            printf("/***************************************---------ev_wm_burn_efuse_end        \n"); break;
        case ev_wm_factory_mode            :
            printf("/***************************************---------ev_wm_factory_mode          \n"); break;
        case ev_wm_block_for_audition      : 
            printf("/***************************************---------ev_wm_block_for_audition    \n"); break;
        case ev_wm_audition_failure        : 
            printf("/***************************************---------ev_wm_audition_failure      \n"); break;
        case ev_wm_burn_audsec_success     : 
            printf("/***************************************---------ev_wm_burn_audsec_success   \n"); break;
        case ev_wm_burn_audsec_failure     : 
            printf("/***************************************---------ev_wm_burn_audsec_failure   \n"); break;
        case ev_wm_chip_poweroff           : 
            printf("/***************************************---------ev_wm_chip_poweroff         \n"); break;
        case ev_wm_ready_next_work         : 
            printf("/***************************************---------ev_wm_ready_next_work       \n"); break;
        case ev_wm_exit_valid              : 
            printf("/***************************************---------ev_wm_exit_valid            \n"); 
            bFinished = true;
            break;
        case ev_wm_exit_invalid            : 
            printf("/***************************************---------ev_wm_exit_invalid          \n"); 
            bFinished = true;
            break;
        case ev_wm_factory_mode_success    :
            printf("/***************************************---------ev_wm_factory_mode_success  \n"); break;
        case ev_wm_factory_mode_progress   : 
            printf("/***************************************---------ev_wm_factory_mode_progress \n"); break;
        case ev_wm_factory_mode_fail       : 
            printf("/***************************************---------ev_wm_factory_mode_fail     \n"); break;
        case ev_wm_factory_calib_value     : 
            printf("/***************************************---------ev_wm_factory_calib_value   \n"); break;
        case ev_wm_exit_user_stop          : 
            printf("/***************************************---------ev_wm_exit_user_stop        \n"); break;
        case ev_wm_read_success            : 
            printf("/***************************************---------ev_wm_read_success          \n"); break;
        case ev_wm_read_fail               : 
            printf("/***************************************---------ev_wm_read_fail             \n"); break;
            //         case ev_wm_max                     :     //default return value.
            //             printf("ev_wm_max                   \n"); break;
        }

        if (bFinished)
        {
            break;
        }
    }
}

void BurnCodeAndCustom()
{
    /****************************** write code and custom area ******************************/
    /** 
    *@brief custom area and code area will be wrote
    *@brief write factory and app.bin
    **/
    const char *pPara[] = {
        "dldtool.exe",   //default parameter  
        "-C37",         //comm 13
        "-R",
        "-e 1",
        //burn flash operated bin. Offered by BES
        "-rE:\\Work\\productline\\download tool\\extend\\transferdll\\Debug\\programmer2300.bin",  

        //factory which contain config information. built by custom
        "-fE:\\Work\\productline\\download tool\\extend\\transferdll\\Debug\\factory.bin",
        
        //apply bin that will be wrote.
        "-bE:\\Work\\productline\\download tool\\extend\\transferdll\\Debug\\best2300_ep.bin",
        //"-bD:\\hscCyberon\\HTS068_i10_2300Z_DEMON_R(2).bin",
        
    };

    dldstart(7, pPara);

    //working threads will be started
    //main thread achieve informations if necessary.
    //monitor event
    
    MonitorDownloadEvent();

    dldstop();

}

int _tmain_4(int argc, _TCHAR* argv[])
{
    /**
     *@brief   flash is divided to factory image(factory custom area) and burn image(code area)
     *@brief   factory image: config informations stored in factory.bin
     *@brief   burn image: FW CODE( App.bin + ota.bin)
     *@brief   all operations are supported by programmer.bin
     */

    //SETP1: analyze app.bin file to achieve information first.
    handle_buildinfo_to_extend("E:\\Work\\productline\\download tool\\extend\\transferdll\\Debug\\best2300_ep.bin");
    
    //called if necessary to build factory.bin file base on app.bin information
    userdata_sector_gen("E:\\Work\\productline\\download tool\\extend\\transferdll\\Debug\\factory.bin");


    char keyBuf[128];
    memset(keyBuf, 0xff, sizeof(keyBuf) );
    //int wrKeyLength = SetLHDCkey(keyBuf, sizeof(keyBuf) );

    char btAddr[6];//address: 12 34 56 78 9a bc
    btAddr[5] = 0x12;
    btAddr[4] = 0x34;
    btAddr[3] = 0x56;
    btAddr[2] = 0x78;
    btAddr[1] = 0x9a;
    btAddr[0] = 0xbc;

    char bleAddr[6]; //address: 12 34 56 78 9a bc
    bleAddr[5] = 0x12;//high address
    bleAddr[4] = 0x34;
    bleAddr[3] = 0x56;
    bleAddr[2] = 0x78;
    bleAddr[1] = 0x9a;
    bleAddr[0] = 0xbc;

    char dangleAddr[6];
    dangleAddr[5] = 0x11;//high address
    dangleAddr[4] = 0x22;
    dangleAddr[3] = 0x33;
    dangleAddr[2] = 0x44;
    dangleAddr[1] = 0x55;
    dangleAddr[0] = 0x66;

    //called if necessary to reset factory area information
    sector_gen("E:\\Work\\productline\\download tool\\extend\\transferdll\\Debug\\factory.bin",
        btAddr, //BT ADDR:
        bleAddr, //BLE ADDR
        dangleAddr, //DANGLE ADDR
        "bt_name",      //BT NAME
        "ble_name",     //BLE NAME
        DEFAULT_CALIBRATE_VALUE);   //CALIBRATE 

    int chipType =  get_chiptype_value();

    BurnCodeAndCustom();
    
	return 0;
}

