// TransferDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <Windows.h>


#include "DldTool.h"


#ifdef _DEBUG
#pragma comment(lib,"transferdll.lib")
#else
#pragma comment(lib,"transferdll.lib")
#endif


enum{
    DEFAULT_CALIBRATE_VALUE = 127,
};

void MonitorDownloadEvent_app()
{
    while (1)
    {
        Sleep(10);
        unsigned long notifyValue =  get_notify_from_cext();
        bool bFinished = false;
        switch(notifyValue)
        {
        case ev_wm_port_open_failed        : 
            printf("/***************************************--------- ev_wm_port_open_failed      \n"); break;
        case ev_wm_port_open_succeed       :
            printf("/***************************************---------ev_wm_port_open_succeed     \n"); break;
        case ev_wm_sync_wait               :
            printf("/***************************************---------ev_wm_sync_wait             \n"); break;
        case ev_wm_sync_failed             :
            printf("/***************************************---------ev_wm_sync_failed           \n"); break;
        case ev_wm_sync_succeed            :
            printf("/***************************************---------ev_wm_sync_succeed          \n"); break;
        case ev_wm_run_programmer_failed   :
            printf("/***************************************---------ev_wm_run_programmer_failed \n"); break;
        case ev_wm_run_programmer_succeed  :
            printf("/***************************************---------ev_wm_run_programmer_succeed\n"); break;
        case ev_wm_bin_encrypt_begin       :
            printf("/***************************************---------ev_wm_bin_encrypt_begin     \n"); break;
        case ev_wm_bin_encrypt_end         :
            printf("/***************************************---------ev_wm_bin_encrypt_end       \n"); break;
        case ev_wm_update_sw_ver           : 
            printf("/***************************************---------ev_wm_update_sw_ver         \n"); break;
        case ev_wm_update_product_id       :
            printf("/***************************************---------ev_wm_update_product_id     \n"); break;
        case ev_wm_burn_progress           :
            printf("/***************************************---------ev_wm_burn_progress         \n"); 
            {
                int iProcess = get_burn_progress();
                //printf("/************************** process:%d********************\n", iProcess);
            }
            break;
        case ev_wm_burn_magic              : 
            printf("/***************************************---------ev_wm_burn_magic            \n"); break;
        case ev_wm_burn_failure            : 
            printf("/***************************************---------ev_wm_burn_failure          \n"); break;
        case ev_wm_burn_complt             :
            printf("/***************************************---------ev_wm_burn_complt           \n");
            bFinished = true;
            break;
        case ev_wm_burn_efuse_start        :
            printf("/***************************************---------ev_wm_burn_efuse_start      \n"); break;
        case ev_wm_burn_efuse_end          : 
            printf("/***************************************---------ev_wm_burn_efuse_end        \n"); break;
        case ev_wm_factory_mode            :
            printf("/***************************************---------ev_wm_factory_mode          \n"); break;
        case ev_wm_block_for_audition      : 
            printf("/***************************************---------ev_wm_block_for_audition    \n"); break;
        case ev_wm_audition_failure        : 
            printf("/***************************************---------ev_wm_audition_failure      \n"); break;
        case ev_wm_burn_audsec_success     : 
            printf("/***************************************---------ev_wm_burn_audsec_success   \n"); break;
        case ev_wm_burn_audsec_failure     : 
            printf("/***************************************---------ev_wm_burn_audsec_failure   \n"); break;
        case ev_wm_chip_poweroff           : 
            printf("/***************************************---------ev_wm_chip_poweroff         \n"); break;
        case ev_wm_ready_next_work         : 
            printf("/***************************************---------ev_wm_ready_next_work       \n"); break;
        case ev_wm_exit_valid              : 
            printf("/***************************************---------ev_wm_exit_valid            \n"); 
            bFinished = true;
            break;
        case ev_wm_exit_invalid            : 
            printf("/***************************************---------ev_wm_exit_invalid          \n"); 
            bFinished = true;
            break;
        case ev_wm_factory_mode_success    :
            printf("/***************************************---------ev_wm_factory_mode_success  \n"); break;
        case ev_wm_factory_mode_progress   : 
            printf("/***************************************---------ev_wm_factory_mode_progress \n"); break;
        case ev_wm_factory_mode_fail       : 
            printf("/***************************************---------ev_wm_factory_mode_fail     \n"); break;
        case ev_wm_factory_calib_value     : 
            printf("/***************************************---------ev_wm_factory_calib_value   \n"); break;
        case ev_wm_exit_user_stop          : 
            printf("/***************************************---------ev_wm_exit_user_stop        \n"); break;
        case ev_wm_read_success            : 
            printf("/***************************************---------ev_wm_read_success          \n"); break;
        case ev_wm_read_fail               : 
            printf("/***************************************---------ev_wm_read_fail             \n"); break;
            //         case ev_wm_max                     :     //default return value.
            //             printf("ev_wm_max                   \n"); break;
        }

        if (bFinished)
        {
            break;
        }
    }
}

void BurnCodeArea()
{
    /****************************** write code area ******************************/
    /** 
    *@brief custom area and code area will be wrote
    *@brief write factory and app.bin
    **/
    const char *pPara[] = {
        "dldtool.exe",   //default parameter  
        "-C37",         //comm 

        //burn flash operated bin. Offered by BES
        "-rE:\\Work\\productline\\download tool\\extend\\transferdll\\Debug\\programmer2300.bin",  

        //apply bin that will be wrote.
        "-bE:\\Work\\productline\\download tool\\extend\\transferdll\\Debug\\best2300_ep.bin",

        "-bE:\\Work\\productline\\download tool\\extend\\transferdll\\Debug\\ota_2300_test.bin"
    };

    dldstart(5, pPara);

    MonitorDownloadEvent_app();

    dldstop();

}

int _tmain����3(int argc, _TCHAR* argv[])
{ 
    BurnCodeArea();
    
	return 0;
}

