// TransferDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <string.h>
#include <Windows.h>



#include "DldTool.h"



#ifdef _DEBUG
#pragma comment(lib,"transferdll.lib")
#else
#pragma comment(lib,"transferdll.lib")
#endif


enum{
    DEFAULT_CALIBRATE_VALUE = 127,
};

void MonitorDownloadEvent_ReadInformations()
{
    while (1)
    {
        Sleep(10);
        unsigned long notifyValue =  get_notify_from_cext();
        bool bFinished = false;
        switch(notifyValue)
        {
        case ev_wm_port_open_failed        : 
            printf("/***************************************--------- ev_wm_port_open_failed      \n"); break;
        case ev_wm_port_open_succeed       :
            printf("/***************************************---------ev_wm_port_open_succeed     \n"); break;
        case ev_wm_sync_wait               :
            printf("/***************************************---------ev_wm_sync_wait             \n"); break;
        case ev_wm_sync_failed             :
            printf("/***************************************---------ev_wm_sync_failed           \n"); break;
        case ev_wm_sync_succeed            :
            printf("/***************************************---------ev_wm_sync_succeed          \n"); break;
        case ev_wm_run_programmer_failed   :
            printf("/***************************************---------ev_wm_run_programmer_failed \n"); break;
        case ev_wm_run_programmer_succeed  :
            printf("/***************************************---------ev_wm_run_programmer_succeed\n"); break;
        case ev_wm_bin_encrypt_begin       :
            printf("/***************************************---------ev_wm_bin_encrypt_begin     \n"); break;
        case ev_wm_bin_encrypt_end         :
            printf("/***************************************---------ev_wm_bin_encrypt_end       \n"); break;
        case ev_wm_update_sw_ver           : 
            printf("/***************************************---------ev_wm_update_sw_ver         \n"); break;
        case ev_wm_update_product_id       :
            printf("/***************************************---------ev_wm_update_product_id     \n"); break;
        case ev_wm_burn_progress           :
            printf("/***************************************---------ev_wm_burn_progress         \n"); 
            {
                int iProcess = get_burn_progress();
                //printf("/************************** process:%d********************\n", iProcess);
            }
            break;
        case ev_wm_burn_magic              : 
            printf("/***************************************---------ev_wm_burn_magic            \n"); break;
        case ev_wm_burn_failure            : 
            printf("/***************************************---------ev_wm_burn_failure          \n"); break;
        case ev_wm_burn_complt             :
            printf("/***************************************---------ev_wm_burn_complt           \n");
            bFinished = true;
            break;
        case ev_wm_burn_efuse_start        :
            printf("/***************************************---------ev_wm_burn_efuse_start      \n"); break;
        case ev_wm_burn_efuse_end          : 
            printf("/***************************************---------ev_wm_burn_efuse_end        \n"); break;
        case ev_wm_factory_mode            :
            printf("/***************************************---------ev_wm_factory_mode          \n"); break;
        case ev_wm_block_for_audition      : 
            printf("/***************************************---------ev_wm_block_for_audition    \n"); break;
        case ev_wm_audition_failure        : 
            printf("/***************************************---------ev_wm_audition_failure      \n"); break;
        case ev_wm_burn_audsec_success     : 
            printf("/***************************************---------ev_wm_burn_audsec_success   \n"); break;
        case ev_wm_burn_audsec_failure     : 
            printf("/***************************************---------ev_wm_burn_audsec_failure   \n"); break;
        case ev_wm_chip_poweroff           : 
            printf("/***************************************---------ev_wm_chip_poweroff         \n"); break;
        case ev_wm_ready_next_work         : 
            printf("/***************************************---------ev_wm_ready_next_work       \n"); break;
        case ev_wm_exit_valid              : 
            printf("/***************************************---------ev_wm_exit_valid            \n"); 
            bFinished = true;
            break;
        case ev_wm_exit_invalid            : 
            printf("/***************************************---------ev_wm_exit_invalid          \n"); 
            bFinished = true;
            break;
        case ev_wm_factory_mode_success    :
            printf("/***************************************---------ev_wm_factory_mode_success  \n"); break;
        case ev_wm_factory_mode_progress   : 
            printf("/***************************************---------ev_wm_factory_mode_progress \n"); break;
        case ev_wm_factory_mode_fail       : 
            printf("/***************************************---------ev_wm_factory_mode_fail     \n"); break;
        case ev_wm_factory_calib_value     : 
            printf("/***************************************---------ev_wm_factory_calib_value   \n"); break;
        case ev_wm_exit_user_stop          : 
            printf("/***************************************---------ev_wm_exit_user_stop        \n"); break;
        case ev_wm_read_success            : 
            printf("/***************************************---------ev_wm_read_success          \n"); break;
        case ev_wm_read_fail               : 
            printf("/***************************************---------ev_wm_read_fail             \n"); break;
            //         case ev_wm_max                     :     //default return value.
            //             printf("ev_wm_max                   \n"); break;
        }

        if (bFinished)
        {
            break;
        }
    }
}

void RumProgrammer_GetInfo_读取工厂区信息()
{
    //第一步：获取芯片分区等信息获
    //需要用到烧录到app.bin 文件。
    //通过分析app.bin 得出bt address, bt name, ble address, ble name, trim(calibration) 地址
    handle_buildinfo_to_extend("C:\\Users\\Jnshell\\Desktop\\TRANSFER_VS1.0.0.3\\TRANSFER\\TransferDll_D\\best2300_ep.bin");

    const char *pPara[] = {
        "dldtool.exe",   //default parameter  
        "-C4",         //comm 

        //burn flash operated bin. Offered by BES
        "-rC:\\Users\\Jnshell\\Desktop\\TRANSFER_VS1.0.0.3\\TRANSFER\\TransferDll_D\\programmer2300.bin",  
    };

    dldstart(3, pPara);

    MonitorDownloadEvent_ReadInformations();

    /************************* 蓝牙名称 ***************************/
    char btName[252];
    memset(btName, 0, sizeof(btName) );
    int bRet =get_BT_Name(btName);
    if (bRet > 0)
    {
        printf("bt_name =%s\n", btName);
    }
    /************************** 蓝牙地址 **************************/
    char btAddr[8];
    memset(btAddr, 0, sizeof(btAddr) );
    bRet =get_BT_Addr(btAddr);
    if (bRet > 0)
    {
        printf("bt_address=%x %x %x %x %x %x\n", 
            btAddr[5]&0xff,
            btAddr[4]&0xff,
            btAddr[3]&0xff,
            btAddr[2]&0xff,
            btAddr[1]&0xff,
            btAddr[0]&0xff);
    }
    /************************** BLE 名称 **************************/
    char bleName[252];
    memset(bleName, 0, sizeof(bleName) );
    bRet =get_BLE_Name(bleName);
    if (bRet > 0)
    {
        printf("ble_name =%s\n", bleName);
    }
    /************************** BLE 地址 **************************/
    char bleAddr[8];
    memset(bleAddr, 0, sizeof(bleAddr) );
    bRet =get_BLE_Addr(bleAddr);
    if (bRet > 0)
    {
        printf("BLE_address=%x %x %x %x %x %x\n", 
            bleAddr[5]&0xff,
            bleAddr[4]&0xff,
            bleAddr[3]&0xff,
            bleAddr[2]&0xff,
            bleAddr[1]&0xff,
            bleAddr[0]&0xff);
    }

    /*************************** 频偏校准值 *************************/
    int iCalibration = get_Calib_Value();
    if (iCalibration > 0)
    {
        printf("calibration =%x\n", iCalibration);
    }

    /*************************** 芯片标识码 *************************/
    unsigned char chipUuidBuf[16];
    memset(chipUuidBuf, 0, sizeof(chipUuidBuf));

    int iChipUuid = get_chip_Uuid(chipUuidBuf);
    if (iChipUuid > 0)
    {
        printf("chip uuid:\n");
        for (int i = 0; i < sizeof(chipUuidBuf); i++)
        {
            printf("%x ", chipUuidBuf[i]);
        }
        printf("\n");
    }

    /****************************************************/
    dldstop();
}

int _tmain_7s(int argc, _TCHAR* argv[])
{
    //注意，使用 十进制数 参数
    RumProgrammer_GetInfo_读取工厂区信息();
	return EXIT_SUCCESS;
}

