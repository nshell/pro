#ifndef _BES_DOWNLOAD_TOOL_H
#define _BES_DOWNLOAD_TOOL_H


// #ifdef TRANSFERDLL_EXPORTS  
#ifdef __cplusplus
extern "C" {
#endif

#ifdef TRANSFERDLL_EXPORTS  
#define _DLL_API _declspec(dllexport)  
#else  
#define _DLL_API _declspec(dllimport)  
#endif   


typedef enum{
    ev_wm_param_parse_failed = 1025,//1025
    ev_wm_port_open_failed =1026,//1026
    ev_wm_port_open_succeed,     //1027
    ev_wm_sync_wait,             //1028
    ev_wm_sync_failed,           //1029
    ev_wm_sync_succeed,          //1030
    ev_wm_run_programmer_failed, //1031
    ev_wm_run_programmer_succeed,//1032
    ev_wm_bin_encrypt_begin,     //1033
    ev_wm_bin_encrypt_end,       //1034
    ev_wm_update_sw_ver,         //1035
    ev_wm_update_product_id,     //1036
    ev_wm_burn_progress,         //1037
    ev_wm_burn_magic,            //1038
    ev_wm_burn_failure,          //1039
    ev_wm_burn_complt,           //1040
    ev_wm_burn_efuse_start,      //1041
    ev_wm_burn_efuse_end,        //1042
    ev_wm_factory_mode,          //1043
    ev_wm_block_for_audition,    //1044
    ev_wm_audition_failure,      //1045
    ev_wm_burn_audsec_success,   //1046
    ev_wm_burn_audsec_failure,   //1047
    ev_wm_chip_poweroff,         //1048
    ev_wm_ready_next_work,       //1049
    ev_wm_exit_valid,            //1050
    ev_wm_exit_invalid,          //1051
    ev_wm_factory_mode_success,    //1052
    ev_wm_factory_mode_progress,    //1053
    ev_wm_factory_mode_fail,        //1054
    ev_wm_factory_calib_value,    //1055
    ev_wm_exit_user_stop,         //1056
    ev_wm_read_success, //1057
    ev_wm_read_fail,  //1058
    ev_wm_max,                    //1059
}tool_wm_ev_enum;

typedef enum{
    BES_CHIP_1000 = 1000,
    BES_CHIP_1400 = 1400,
    BES_CHIP_2000 = 2000,
    BES_CHIP_2300 = 2300,

}BES_CHIP;


typedef enum{
    NO_WR_SN_FLAG = 0X01 << 6,  //1: execute operation 0：use chip information
    NO_WR_BT_ADDR = 0X01 << 5,  //1: execute operation 0：use chip information
    NO_WR_BT_NAME = 0X01 << 4,  //1: execute operation 0：use chip information
    NO_WR_BLE_ADDR = 0X01 << 3, //1: execute operation 0：use chip information
    NO_WR_BLE_NAME = 0X01 << 2, //1: execute operation 0：use chip information
    NO_WR_CON_ADDR = 0X01 << 1, //1: execute operation 0：use chip information
    NO_WR_CALIBRAT = 0X01 << 0, //1: execute operation 0：use chip information
}WR_INFO_FLAG;

/****************************************************/

_DLL_API void   handle_buildinfo_to_extend(char *app_bin);


/**
*@brief Generate facotory.bin base on parameters
*@parameter *bin_path factory.bin file path
*@parameter *param_btaddr bluetooth address
*@parameter *param_bleaddr BLE address
*@parameter *param_dongleaddr dongle address
*@parameter *dev_bt_name bluetooth name
*@parameter *dev_ble_name BLE name
*@parameter default_xtal_fcap default value of calibrated
*@return -1:failed. 0: success
*/
_DLL_API int sector_gen(char *bin_path, char *param_btaddr, char *param_bleaddr, char *param_dongleaddr, char *dev_bt_name, char *dev_ble_name, unsigned int default_xtal_fcap);

/**
*@brief     Generate facotory.bin
*@parameter bin_path directory for factory.bin
*@return    -1:failed
*@return    0: success
*/
_DLL_API int userdata_sector_gen(char *bin_path);


/**
*@return percent that has been burned.
*@parameter  param_argc count of parameter pointer
*@parameter  param_argv pointer that point to parameter array
*@brief    -C       COM 
                    //串口号
*@brief    -r       programmer.bin
                    //BES 提供的特定芯片型号的RAMRUN.1000/2000/2300/1400

*@brief    -f       factory image(factory.bin) that will be download
                    //烧录修改工厂区需要指定工厂区文件

*@brief    -b       burn image(app.bin) that will be download   
                    //耳机 app.bin / OTA.bin 文件参数，可以同时两个这个参数

*@brief    -B       custom bin
                    //在指定地址上存放改 bin 文件。文件末尾4byte 需要指定该地址  

*@brief    -F       Factory mode flag   // 1 is auto entry factory mode ,0 is ignore factory mode after burn flash.
*@brief    -c       Calibrate flag     // 1 is auto entry calibrated mode ,0 is ignore calibrated mode after burn flash.

*@brief    -P       if need chip power off when burn completed
                    //烧录完后，发送 shutDown 关机指令

*@brief    -e       erase chip flag     
                    //设置 1("-e 1") 擦除整个flash

*@brief    -w       modified information flag,default value:0x7f(related informations modified). see enum WR_INFO_FLAG
                    //选择性的修改工厂区信息。默认是 0x7f 都修改。 （"-w 1" 只修改bit 1 表示的trim 校准值）
*/
_DLL_API int    dldstart(int param_argc,const char **param_argv);

_DLL_API void   dldstop(void);

/**
*@return percent that has been burned.
*/
_DLL_API int get_burn_progress(void);


/**
*@return achieve error code when error happed
*/
_DLL_API unsigned int get_exit_code(void);


/**
*@brief calibrate process percent //?
*@return percent value
*/
_DLL_API int pyext_get_mcu_test_progress(void);

/**
*@brief achieve chip calibrated value
*@return calibrated value.
*/
_DLL_API int pyext_get_calib_value(void);

/**
*@brief Get download message(tool_wm_ev_enum) from port
*@brief thread id input before start
*@return message value.(tool_wm_ev_enum)
*/
_DLL_API unsigned long get_notify_from_cext(void);


/**
*@brief verify that the Bluetooth address of the LAP
*@return message value.(tool_wm_ev_enum)
*/
_DLL_API int    verify_lap_is_proper(unsigned int lap);


/**
*@brief set chip type：1000/2000/2300/1400
*/
_DLL_API void   set_chiptype_value(BES_CHIP chiType);

/**
*@brief get chip type：1000/2000/2300/1400
*@return type
*/
_DLL_API int    get_chiptype_value(void);

/**
*@brief achieve FW version
*@return version
*/
_DLL_API int    get_version(void);
  

/**
*@brief achieve BLE Name
*@return 0: failed. 1:succeed
*/
_DLL_API int    get_BLE_Name(char *pDstBuf);

/**
*@brief achieve BLE address
*@return 0: failed. 1:succeed
*/
_DLL_API int    get_BLE_Addr(char *pDstBuf);

/**
*@brief achieve BT NAME
*@return 0: failed. 1:succeed
*/
_DLL_API int    get_BT_Name(char *pDstBuf);

/**
*@brief achieve BT Address
*@return 0: failed. 1:succeed
*/
_DLL_API int    get_BT_Addr(char *pDstBuf);


 /**
 *@brief achieve trim calibration version
 *@return -1: failed.
 */
 _DLL_API int    get_Calib_Value(void);

 /**
 *@brief achieve flash universally unique identify
 *@return 0: failed. 1: ok
 */
 _DLL_API int    get_chip_Uuid(unsigned char *pUuidBuf);

#ifdef __cplusplus
};
#endif



// #endif  //TRANSFERDLL_EXPORTS


/*********************************************************/
#endif

