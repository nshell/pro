// TransferDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <Windows.h>

#include "DldTool.h"


#ifdef _DEBUG
#pragma comment(lib,"transferdll.lib")
#else
#pragma comment(lib,"transferdll.lib")
#endif


enum{
    DEFAULT_CALIBRATE_VALUE = 127,
};

void MonitorDownloadEvent_ModifyTrim()
{
    while (1)
    {
        Sleep(10);
        unsigned long notifyValue =  get_notify_from_cext();
        bool bFinished = false;
        switch(notifyValue)
        {
        case ev_wm_port_open_failed        : 
            printf("/***************************************--------- ev_wm_port_open_failed      \n"); break;
        case ev_wm_port_open_succeed       :
            printf("/***************************************---------ev_wm_port_open_succeed     \n"); break;
        case ev_wm_sync_wait               :
            printf("/***************************************---------ev_wm_sync_wait             \n"); break;
        case ev_wm_sync_failed             :
            printf("/***************************************---------ev_wm_sync_failed           \n"); break;
        case ev_wm_sync_succeed            :
            printf("/***************************************---------ev_wm_sync_succeed          \n"); break;
        case ev_wm_run_programmer_failed   :
            printf("/***************************************---------ev_wm_run_programmer_failed \n"); break;
        case ev_wm_run_programmer_succeed  :
            printf("/***************************************---------ev_wm_run_programmer_succeed\n"); break;
        case ev_wm_bin_encrypt_begin       :
            printf("/***************************************---------ev_wm_bin_encrypt_begin     \n"); break;
        case ev_wm_bin_encrypt_end         :
            printf("/***************************************---------ev_wm_bin_encrypt_end       \n"); break;
        case ev_wm_update_sw_ver           : 
            printf("/***************************************---------ev_wm_update_sw_ver         \n"); break;
        case ev_wm_update_product_id       :
            printf("/***************************************---------ev_wm_update_product_id     \n"); break;
        case ev_wm_burn_progress           :
            printf("/***************************************---------ev_wm_burn_progress         \n"); 
            {
                int iProcess = get_burn_progress();
                //printf("/************************** process:%d********************\n", iProcess);
            }
            break;
        case ev_wm_burn_magic              : 
            printf("/***************************************---------ev_wm_burn_magic            \n"); break;
        case ev_wm_burn_failure            : 
            printf("/***************************************---------ev_wm_burn_failure          \n"); break;
        case ev_wm_burn_complt             :
            printf("/***************************************---------ev_wm_burn_complt           \n");
            bFinished = true;
            break;
        case ev_wm_burn_efuse_start        :
            printf("/***************************************---------ev_wm_burn_efuse_start      \n"); break;
        case ev_wm_burn_efuse_end          : 
            printf("/***************************************---------ev_wm_burn_efuse_end        \n"); break;
        case ev_wm_factory_mode            :
            printf("/***************************************---------ev_wm_factory_mode          \n"); break;
        case ev_wm_block_for_audition      : 
            printf("/***************************************---------ev_wm_block_for_audition    \n"); break;
        case ev_wm_audition_failure        : 
            printf("/***************************************---------ev_wm_audition_failure      \n"); break;
        case ev_wm_burn_audsec_success     : 
            printf("/***************************************---------ev_wm_burn_audsec_success   \n"); break;
        case ev_wm_burn_audsec_failure     : 
            printf("/***************************************---------ev_wm_burn_audsec_failure   \n"); break;
        case ev_wm_chip_poweroff           : 
            printf("/***************************************---------ev_wm_chip_poweroff         \n"); break;
        case ev_wm_ready_next_work         : 
            printf("/***************************************---------ev_wm_ready_next_work       \n"); break;
        case ev_wm_exit_valid              : 
            printf("/***************************************---------ev_wm_exit_valid            \n"); 
            bFinished = true;
            break;
        case ev_wm_exit_invalid            : 
            printf("/***************************************---------ev_wm_exit_invalid          \n"); 
            bFinished = true;
            break;
        case ev_wm_factory_mode_success    :
            printf("/***************************************---------ev_wm_factory_mode_success  \n"); break;
        case ev_wm_factory_mode_progress   : 
            printf("/***************************************---------ev_wm_factory_mode_progress \n"); break;
        case ev_wm_factory_mode_fail       : 
            printf("/***************************************---------ev_wm_factory_mode_fail     \n"); break;
        case ev_wm_factory_calib_value     : 
            printf("/***************************************---------ev_wm_factory_calib_value   \n"); break;
        case ev_wm_exit_user_stop          : 
            printf("/***************************************---------ev_wm_exit_user_stop        \n"); break;
        case ev_wm_read_success            : 
            printf("/***************************************---------ev_wm_read_success          \n"); break;
        case ev_wm_read_fail               : 
            printf("/***************************************---------ev_wm_read_fail             \n"); break;
            //         case ev_wm_max                     :     //default return value.
            //             printf("ev_wm_max                   \n"); break;
        }

        if (bFinished)
        {
            break;
        }
    }
}

void Modify_Factory_仅仅修改校准值()
{
    //SETP1: analyze app.bin file to achieve information first.
    //第一步：获取芯片烧写分区等信息获
    handle_buildinfo_to_extend("C:\\Users\\Jnshell\\Desktop\\TRANSFER_VS1.0.0.3\\TRANSFER\\TransferDll_D\\best2300_ep.bin");
    
    //SETP1:build factory.bin file base on app.bin informations
    //第二步：创建工厂区bin 文件
    userdata_sector_gen("C:\\Users\\Jnshell\\Desktop\\TRANSFER_VS1.0.0.3\\TRANSFER\\TransferDll_D\\factory.bin");

    char btAddr[6];//address: 12 34 56 78 9a bc
    btAddr[5] = 0x12;
    btAddr[4] = 0x34;
    btAddr[3] = 0x56;
    btAddr[2] = 0x78;
    btAddr[1] = 0x9a;
    btAddr[0] = 0xbc;

    char bleAddr[6]; //address: 12 34 56 78 9a bc
    bleAddr[5] = 0x12;//high address
    bleAddr[4] = 0x34;
    bleAddr[3] = 0x56;
    bleAddr[2] = 0x78;
    bleAddr[1] = 0x9a;
    bleAddr[0] = 0xbc;

    char dangleAddr[6];
    dangleAddr[5] = 0x11;//high address
    dangleAddr[4] = 0x22;
    dangleAddr[3] = 0x33;
    dangleAddr[2] = 0x44;
    dangleAddr[1] = 0x55;
    dangleAddr[0] = 0x66;

    //step3：set factory informations into bin file
    //第三步：设置对应芯片的信息到bin 文件里面
    // 注意 ：对应的接口参数还是需要填写值
    sector_gen("C:\\Users\\Jnshell\\Desktop\\TRANSFER_VS1.0.0.3\\TRANSFER\\TransferDll_D\\factory.bin",
        btAddr, //BT ADDR:
        bleAddr, //BLE ADDR
        dangleAddr, //DANGLE ADDR
        "bt_name",      //BT NAME
        "ble_name",     //BLE NAME
        0x88);   //CALIBRATE 

    //step4：start burned process
    //第四步：开启烧录信息的流程
    const char *pPara[] = {
        "dldtool.exe",   //default parameter  
        "-C4",         //comm 13
        //burn flash operated bin. Offered by BES
        "-rC:\\Users\\Jnshell\\Desktop\\TRANSFER_VS1.0.0.3\\TRANSFER\\TransferDll_D\\programmer2300.bin",  

        //factory which contain config information. built by custom
        "-fC:\\Users\\Jnshell\\Desktop\\TRANSFER_VS1.0.0.3\\TRANSFER\\TransferDll_D\\factory.bin",
        "-w 1",  //通过 bit 0 位置1，其他bit 位置零来控制仅仅修改 校准频偏 值。
        "-e 1",     //
   };

    dldstart(5, pPara);

    //working threads will be started
    //main thread achieve informations if necessary.
    //monitor event
    MonitorDownloadEvent_ModifyTrim();

    dldstop();
}
int _tmain_3(int argc, _TCHAR* argv[])
{
    //注意，使用 十进制数 参数
    Modify_Factory_仅仅修改校准值();
	return 0;
}

