#pragma once
class display_info
{
	public:
		CString Device_state[10];
		CString Scanner_state[10];
		int Burn_Progress[10];
		CString Ble_Addr[10];
		CString Bt_Addr[10];
		CString Dangle_Addr[10];
		CString Ble_Name[10];
		CString Bt_Name[10];
		CString Dangle_Name[10];
		CString Bin_path;
		CString Ota_path;
		boolean Is_erase[10];
		boolean Is_RTS_Enable;
		boolean Is_Pow_Enable;
		boolean Is_Burn_Enable;
		boolean Is_Fac_Enable;
		boolean Is_Log1_Enable;
		boolean Is_Log2_Enable;
		boolean Is_Log3_Enable;
		boolean Is_Log4_Enable;
		int Developer_Mode;
		void EnableAllBtn();
		void DisableAllBtn();
};

