
// proDlg.h : 头文件
//

#pragma once
#include "afxcmn.h"
#include "DldTool.h"
#include "Resource.h"
#include "Devicelog.h"
#include <ShellScalingAPI.h>
#include <Dbt.h>
#include "scanner.h"



#define DEVICE1	TEXT("VID_0ACE+PID_ACE1")
#define DEVICE2	TEXT("VID_0ACE+PID_ACE2")
#define DEVICE3	TEXT("VID_0ACE+PID_ACE3")
#define DEVICE4	TEXT("VID_0ACE+PID_ACE4")
#define SWITCH	TEXT("VID_0ACE+PID_ACE5")
#define SCAN_BLUE TEXT("VID_0525&PID_A4A7")
#define CBUS0	0xf1
#define CBUS1	0xf2
#define CBUS2	0xf4
#define CBUS3	0xf8
#define D0	0x01
#define D1	0x02
#define D2	0x04
#define D3	0x08
#define D4	0x10
#define D5	0x20
#define D6	0x40
#define D7	0x80
#define RTS	TEXT("RTS")
#define DTR	TEXT("DTR")
#define CTS	TEXT("CTS")
#define DSR	TEXT("DSR")
#define DCD	TEXT("DCD")
#define RI	TEXT("RI")
#define PRO_MODE 100
#define POW_MODE 101
#define RTS_MODE 102
#define FAC_MODE 103
#define DEFAULT_MODE 0
#define BURN_PREPAR 12580



// CproDlg 对话框
class CproDlg : public CDialogEx
{
// 构造
public:
	CproDlg(CWnd* pParent = NULL);	// 标准构造函数
	~CproDlg();

// 对话框数据
	enum { IDD = IDD_PRO_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;


	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedPro();


	CProgressCtrl prog;
	CString csName;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	int DPI_SCALE ;
	float rate=0;
	UINT Developer_Mode = 0;
	HDEVNOTIFY  m_hDevNotify;


private:
	CWinThread* Channel_Thread;
	CWinThread* Channe2_Thread;
	CWinThread* Channe3_Thread;
	CWinThread* Channe4_Thread;
	CWinThread* Channe5_Thread;
	CWinThread* Channe6_Thread;	//扫码枪线程


public:
	CEdit  Bt_addr1, Bt_addr2, Bt_addr3, Bt_addr4 ;
	CEdit  Ble_addr1, Ble_addr2, Ble_addr3, Ble_addr4;
	HACCEL m_hAccel;
	afx_msg void OnEnChangeBtaddr0();
	afx_msg void OnEnChangename();
	afx_msg void OnBnClickedPow();
	CProgressCtrl prog2;
	CProgressCtrl prog3;
	CProgressCtrl prog4;
	afx_msg void OnBnClickedChoosebin();
	afx_msg void OnBnClickedChooseota();
	afx_msg void OnBnClickedRts();
	afx_msg void OnBnClickedFac();
	afx_msg void OnAcceleratorCtrlD();
	virtual BOOL   PreTranslateMessage(MSG* pMsg);

	afx_msg void OnSize(UINT nType, int cx, int cy);
	void ReSize(void);
	POINT old;
	afx_msg void OnBnClickedUpdate();
	afx_msg void OnBnClickedErase();
	void ShowBalloonTip(HWND hWnd, LPCTSTR lpszText, LPCTSTR lpszTittle, int nTTIcon);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg BOOL OnDeviceChange(UINT nEventType, DWORD dwData);
	afx_msg void OnBnClickedLog1();
	afx_msg void OnBnClickedLog2();
	afx_msg void OnBnClickedLog3();
	afx_msg void OnBnClickedLog4();
	UINT Pass_ScannerReading_to_BTBLEAddr(CString str);	//in scanner.cpp
	UINT Reset_BTBLEAddr(LPVOID pParam);
};

UINT  Channel_WorkForce(LPVOID lpParameter);
UINT  Channe2_WorkForce(LPVOID lpParameter);
UINT  Channe3_WorkForce(LPVOID lpParameter);
UINT  Channe4_WorkForce(LPVOID lpParameter);
UINT  Channe5_WorkRest(LPVOID lpParameter);
UINT  Channe6_WorkForce(LPVOID lpParameter);
char* btaddr_conver(CString device, char btaddr[12]);
bool MonitorDownloadEvent_CalibrateTrimAuto(CString device, int notifyValue, int iProcess);
void BurnCalibrateTrimAuto(CString device, CString COM, CString btAddr, CString bleAddr, CString dangleAddr, CString bt_name, CString ble_name, CString path_app_bin, CString path_factory_bin, CString path_programmer_bin, CString path_ota_bin);
int MTGetPortFromVidPid(CString strVidPid);
int Burn_Progress(CString device, CString path_app_bin, CString path_factory_bin, CString path_programmer_bin, CString path_ota_bin);
UINT Poweron(CString device);
UINT RTS_Process(CString device);
UINT FAS_Process(CString device);

