// scanner.cpp : 实现文件
//
#include "scanner.h"
#include "stdafx.h"
#include "afxdialogex.h"
#include "display_info.h"
#include "proDlg.h"
#include <string>
#include <iostream>

extern display_info INFO_STATE;
extern int burn_complete_num;

UINT Pass_COM_to_ScannerReading(CString scanner) 
{
	int scanner_id = -1;
	if (scanner == SCAN_BLUE) scanner_id = 0;

	int nport = -1;
	CString COM = TEXT("COM");
	nport = MTGetPortFromVidPid(scanner);

	if (nport == -1)
	{
		INFO_STATE.Scanner_state[scanner_id] = TEXT("状态：无设备");
	}
	else {
		COM.Format(TEXT("\\\\.\\COM%d"), nport);
		HANDLE	hCom;
		hCom = CreateFile(COM, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
		if (hCom == (HANDLE)-1)
		{
			INFO_STATE.Scanner_state[scanner_id] = TEXT("状态：串口打开失败");
			CloseHandle(hCom);
			return FALSE;
		}
		else
		{
			COMMTIMEOUTS TimeOuts;
			//设定读超时
			TimeOuts.ReadIntervalTimeout = MAXDWORD;
			TimeOuts.ReadTotalTimeoutMultiplier = 0;
			TimeOuts.ReadTotalTimeoutConstant = 0;
			//在读一次输入缓冲区的内容后读操作就立即返回，
			//而不管是否读入了要求的字符。


			//设定写超时
			TimeOuts.WriteTotalTimeoutMultiplier = 100;
			TimeOuts.WriteTotalTimeoutConstant = 500;
			SetCommTimeouts(hCom, &TimeOuts); //设置超时
			SetupComm(hCom, 1024, 1024); //输入缓冲区和输出缓冲区的大小都是1024
			DCB wdcb;
			GetCommState(hCom, &wdcb);
			wdcb.BaudRate = 9600;//波特率：9600，其他：不变
			wdcb.ByteSize = 8;   //每个字节8位
			wdcb.StopBits = ONESTOPBIT;
			wdcb.Parity = NOPARITY;  //无停止位
			SetCommState(hCom, &wdcb);
			PurgeComm(hCom, PURGE_TXCLEAR | PURGE_RXCLEAR);
		}

		char str[1000] = "0";
		BOOL bReadStat = FALSE;
		COMSTAT ComStat;
		DWORD dwErrorFlags;

		while (true)
		{
			DWORD dwBytesRead = 1000;
			ClearCommError(hCom, &dwErrorFlags, &ComStat);
			dwBytesRead = min(dwBytesRead, (DWORD)ComStat.cbInQue);

			if (!dwBytesRead) {
				Sleep(10);
				//continue; //使用continue时，打开串口后CPU占用率非常高
			}
			else {
				bReadStat = ReadFile(hCom, str, 1000, &dwBytesRead, NULL);
				if (!bReadStat) {
					printf("读串口失败！");
					INFO_STATE.Scanner_state[scanner_id] = TEXT("状态：串口读取失败");
					break;

				}
				else {
					printf("&s", str);
					INFO_STATE.Scanner_state[scanner_id] = str;
				}
			}
		}
		CloseHandle(hCom);
	}

	return 0;
}

BOOL device1_flag = FALSE;
BOOL device4_flag = FALSE;
CString prev_scanner_reading;
UINT CproDlg::Pass_ScannerReading_to_BTBLEAddr(CString str)
{
	if (prev_scanner_reading != str) {
		prev_scanner_reading = str;

		//判断地址格式
		if (int length = str.GetLength() != 18 || str.GetAt(2)!=':'|| str.GetAt(5) != ':'|| str.GetAt(8) != ':'|| str.GetAt(11) != ':'|| str.GetAt(14) != ':'){
			this->GetDlgItem(IDC_SCANNER)->SetWindowText(TEXT("地址格式错误: ") + str);	//for testing
			return 0;
		}
		else {
			//判断地址末位的奇偶
			char last_digit = str.GetAt(str.GetLength() - 2);
			std::string odd_number = "13579BDF";
			std::string even_number = "02468ACE";

			if (even_number.find(last_digit) != std::string::npos) { //如果为偶数，更新到device4
				if (device4_flag == FALSE) {
					this->GetDlgItem(IDC_BTADDR4)->SetWindowText(str);
					this->GetDlgItem(IDC_BLEADDR4)->SetWindowText(str);
					device4_flag = TRUE;
				}
				else {
					this->GetDlgItem(IDC_SCANNER)->SetWindowText(TEXT("设备4未完成烧录: ") + str);	//for testing
					return 0;
				}
			}
			else if (odd_number.find(last_digit) != std::string::npos) { //如果为奇数，更新到device1
				if (device1_flag == FALSE) {
					this->GetDlgItem(IDC_BTADDR1)->SetWindowText(str);
					this->GetDlgItem(IDC_BLEADDR1)->SetWindowText(str);
					device1_flag = TRUE;
				}
				else {
					this->GetDlgItem(IDC_SCANNER)->SetWindowText(TEXT("设备1未完成烧录: ") + str);	//for testing
					return 0;
				}
			}
		}
		this->GetDlgItem(IDC_SCANNER)->SetWindowText(TEXT("Scanner Reading: ") + str);	//for testing

	}
	
	return 0;
}


UINT CproDlg::Reset_BTBLEAddr(LPVOID pParam) 
{
	for (int i = 0; i < 4; i++)
	{
		this->GetDlgItem(IDC_BTADDR1 + i)->SetWindowText(TEXT("0"));
		this->GetDlgItem(IDC_BLEADDR1 + i)->SetWindowText(TEXT("0"));
	}

	return 0;
}