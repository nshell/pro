﻿
// pro.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "pro.h"
#include "proDlg.h"
#include "DldTool.h"
#include <stdlib.h>
#include <Windows.h>
#include <stdio.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif





// CproApp

BEGIN_MESSAGE_MAP(CproApp, CWinApp)
END_MESSAGE_MAP()


// CproApp 构造

CproApp::CproApp()
{
	
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;
}


// 唯一的一个 CproApp 对象

CproApp theApp;




BOOL CproApp::InitInstance()
{


	CWinApp::InitInstance();


	AfxEnableControlContainer();


	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));
	CproDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO:  在此放置处理何时用
		//  “确定”来关闭对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO:  在此放置处理何时用
		//  “取消”来关闭对话框的代码
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "警告: 对话框创建失败，应用程序将意外终止。\n");
	}


	return FALSE;
}

BOOL GPIO_SetCBUSConfig(CString device, UCHAR GPIONAME, int state)
{

	FT_STATUS ftStatus;
	FT_HANDLE ftHandle;

	FT_DEVICE_LIST_INFO_NODE* devInfo;
	DWORD numDevs, nodevice_flag = 0;
	char* device_des=" ";

	DWORD w_data_len = 7; // write 7 bytes
	DWORD data_written; // number of bytes written
	UCHAR Mask = 0xff; // all output (output (1) and input (0))
	UCHAR Mode = 0x01; // 0x01 = asynchronous bit-bang
	UCHAR STATE;
	if (device == DEVICE1)device_des = "DEVICE1";
	if (device == DEVICE2)device_des = "DEVICE2";
	if (device == DEVICE3)device_des = "DEVICE3";
	if (device == DEVICE4)device_des = "DEVICE4";
	if (device == SWITCH) device_des = "SWITCH";


					ftStatus = FT_OpenEx(device_des, FT_OPEN_BY_DESCRIPTION, &ftHandle);
					if (ftStatus != FT_OK)
					{
						printf("ftStatus not ok %d\n", ftStatus); //check for error
						return false;
					}
					else {


						
							Mode = 0x20;
							FT_GetBitMode(ftHandle, &STATE);
							Mask = STATE;
							Mask = Mask | 0XF0;

					    if (state == 1) Mask = Mask | GPIONAME;	
						if (state == 0) Mask = Mask & (~GPIONAME | 0xf0);
						ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);

						FT_Close(ftHandle);
						return true;
					}


}


BOOL GPIO_SetDxConfig(CString device, UCHAR GPIONAME, int state)
{

	FT_STATUS ftStatus;
	FT_HANDLE ftHandle;

	FT_DEVICE_LIST_INFO_NODE* devInfo;
	DWORD numDevs, reset_flag = 0;
	char* device_des = " ";
	DWORD w_data_len = 7; // write 7 bytes
	DWORD data_written; // number of bytes written
	UCHAR Mask = 0xff; // all output (output (1) and input (0))
	UCHAR Mode = 0x01; // 0x01 = asynchronous bit-bang
	UCHAR STATE;
	if (device == DEVICE1)device_des = "DEVICE1";
	if (device == DEVICE2)device_des = "DEVICE2";
	if (device == DEVICE3)device_des = "DEVICE3";
	if (device == DEVICE4)device_des = "DEVICE4";
	if (device == SWITCH) device_des = "SWITCH";
	byte data_out[7]; // data to send 7 bytes of alternate 0x55 and 0xAA
	byte data_read[7];


	
					ftStatus = FT_OpenEx(device_des, FT_OPEN_BY_DESCRIPTION, &ftHandle);
					ftStatus |= FT_SetUSBParameters(ftHandle, 4096, 4096); // Set USB transfer sizes
					ftStatus |= FT_SetChars(ftHandle, false, 0, false, 0); // Disable event characters
					ftStatus |= FT_SetTimeouts(ftHandle, 5000, 5000); // Set read/write timeouts to 5 sec
					ftStatus |= FT_SetLatencyTimer(ftHandle, 16); // Latency timer at default 16ms
					ftStatus |= FT_SetFlowControl(ftHandle, FT_FLOW_NONE, 0x11, 0x13);
					ftStatus |= FT_SetBaudRate(ftHandle, 62500); //bit rate is x16 this value = 1M
					if (ftStatus != FT_OK)
					{
						printf("ftStatus not ok %d\n", ftStatus); //check for error
						return false;
					}
					else {

						ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
						FT_Read(ftHandle, data_read, w_data_len, &data_written);
						int x;
						for (x = 0; x < 7; x++) { // set even data_out values to 0x55
							data_out[x] = data_read[x];
						}

						 if (state == 1) data_out[6] = data_out[6] | GPIONAME;
						 if (state == 0) data_out[6] = data_out[6] & (~GPIONAME);
			
						ftStatus = FT_Write(ftHandle, data_out, w_data_len, &data_written);
						if ((ftStatus == FT_OK) && (w_data_len == data_written)) printf("FT_Write successful\n");



						FT_Close(ftHandle);
						return true;
					}


	
}

BOOL SETCOMOUTPUT(CString device, CString FLOW_CONTROL_PIN, DWORD sleeptime_ms)
{
	int nport = -1;
	CString COM = TEXT("COM");

	nport = MTGetPortFromVidPid(device);
	if (nport == -1)
	{
		return false;
	}
	else {
		COM.Format(TEXT("\\\\.\\COM%d"), nport);
		HANDLE	hCom;
		hCom = CreateFile(COM, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);  //COM3这里可以弄一个combobox选择
		if (hCom == (HANDLE)-1)
		{
			return false;
		}
		else
		{

			DCB wdcb;
			GetCommState(hCom, &wdcb);
			wdcb.BaudRate = 921600;//波特率：9600，其他：不变
			wdcb.ByteSize = 8;   //每个字节8位
			wdcb.StopBits = ONESTOPBIT;
			wdcb.Parity = NOPARITY;  //无停止位
			if (FLOW_CONTROL_PIN == RTS)wdcb.fRtsControl = RTS_CONTROL_ENABLE;
			if (FLOW_CONTROL_PIN == DTR)wdcb.fDtrControl = DTR_CONTROL_ENABLE;
			SetCommState(hCom, &wdcb);
			Sleep(sleeptime_ms);
			if (FLOW_CONTROL_PIN == RTS)wdcb.fRtsControl = RTS_CONTROL_DISABLE;
			if (FLOW_CONTROL_PIN == DTR)wdcb.fDtrControl = DTR_CONTROL_DISABLE;
			SetCommState(hCom, &wdcb);
			CloseHandle(hCom);
			return true;
		}
	}
}

BOOL GETCOMINTPUT(CString device, CString FLOW_CONTROL_PIN, LPDWORD state)
{
	int nport = -1;
	CString COM = TEXT("COM");

	nport = MTGetPortFromVidPid(device);
	if (nport == -1)
	{
		return false;
	}
	else {
		COM.Format(TEXT("\\\\.\\COM%d"), nport);
		HANDLE	hCom;
		DWORD status=0;
		hCom = CreateFile(COM, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);  //COM3这里可以弄一个combobox选择
		if (hCom == (HANDLE)-1)
		{
			return false;
		}
		else
		{

			
			DCB wdcb;
			GetCommState(hCom, &wdcb);
			wdcb.BaudRate = 921600;//波特率：9600，其他：不变
			wdcb.ByteSize = 8;   //每个字节8位
			wdcb.StopBits = ONESTOPBIT;
			wdcb.Parity = NOPARITY;  //无停止位
			SetCommState(hCom, &wdcb);
			GetCommModemStatus(hCom, state);
			CloseHandle(hCom);
		
			
			return true;
		}
	}
}