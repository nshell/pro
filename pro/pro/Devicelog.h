﻿#pragma once


// CDevicelog 对话框
#define WM_READCOMM (WM_USER + 1)

class CDevicelog : public CDialogEx
{
	DECLARE_DYNAMIC(CDevicelog)

public:
	CDevicelog(CWnd* pParent,int id);   // 标准构造函数
	 ~CDevicelog();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_LOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
private:
	int key_to_room;
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	BOOL OpenComm(CString Device, int nBaud, int nData, int nStop, int nCal);
	BOOL ReadComm();
	CString DataRX;
	CString LOG_STATE;
	BOOL COMISOK;
	afx_msg void OnClose();
	COMSTAT ComStat;
	DWORD dwErrorFlags;
	HANDLE hCom;
	afx_msg void OnBnClickedBtnsend();
};





