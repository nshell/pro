
// pro.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号
#include "proDlg.h"
#include "ftd2xx.h"
#include <setupapi.h>


#ifdef _DEBUG
#pragma comment(lib,"transferdll.lib")
#pragma comment(lib, "setupapi.lib")
#else
#pragma comment(lib,"transferdll.lib")
#pragma comment(lib, "setupapi.lib")
#endif
// CproApp: 
// 有关此类的实现，请参阅 pro.cpp
//

class CproApp : public CWinApp
{
public:
	CproApp();

// 重写
public:
	virtual BOOL InitInstance();


// 实现

	DECLARE_MESSAGE_MAP()
};

extern CproApp theApp;

BOOL GPIO_SetCBUSConfig(CString device, UCHAR GPIONAME, int state);
BOOL GPIO_SetDxConfig(CString device, UCHAR GPIONAME, int state);
BOOL SETCOMOUTPUT(CString device, CString FLOW_CONTROL_PIN, DWORD sleeptime_ms);
BOOL GETCOMINTPUT(CString device, CString FLOW_CONTROL_PIN, LPDWORD state);