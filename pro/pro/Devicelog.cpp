﻿// Devicelog.cpp: 实现文件
//

#include "stdafx.h"
//#include "pch.h"
#include "pro.h"
#include "Devicelog.h"
#include "afxdialogex.h"
#include "proDlg.h"

// CDevicelog 对话框
IMPLEMENT_DYNAMIC(CDevicelog, CDialogEx)
CDevicelog::CDevicelog(CWnd* pParent, int id/*=nullptr*/)
	: CDialogEx(IDD_DIALOG_LOG, pParent)
{
	key_to_room = id;
	DataRX = "";
	COMISOK = FALSE;
}

CDevicelog::~CDevicelog()
{

}

void CDevicelog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDevicelog, CDialogEx)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BTNSEND, &CDevicelog::OnBnClickedBtnsend)
END_MESSAGE_MAP()


// CDevicelog 消息处理程序

BOOL CDevicelog::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	switch (key_to_room) {
	case 1:
		this->GetDlgItem(IDC_LOG_ID)->SetWindowText(TEXT("Device1"));
		break;
	case 2:
		this->GetDlgItem(IDC_LOG_ID)->SetWindowText(TEXT("Device2"));
		break;
	case 3:
		this->GetDlgItem(IDC_LOG_ID)->SetWindowText(TEXT("Device3"));
		break;
	case 4:
		this->GetDlgItem(IDC_LOG_ID)->SetWindowText(TEXT("Device4"));
		break;
	}
	SetTimer(0, 50, NULL);//50ms
	BOOL COM_IS_OPEN = FALSE;
	int open_count = 0;
	if (key_to_room == 1) 
		while (!COM_IS_OPEN) {
			COM_IS_OPEN = OpenComm(DEVICE1, 921600, 8, ONESTOPBIT, NOPARITY);//尝试打开串口
			open_count++;
			if (open_count > 10)
				break;
		}
	if (key_to_room == 2)
		while (!COM_IS_OPEN) {
			COM_IS_OPEN = OpenComm(DEVICE2, 921600, 8, ONESTOPBIT, NOPARITY);//尝试打开串口
			open_count++;
			if (open_count > 10)
				break;
		}
	if (key_to_room == 3)
		while (!COM_IS_OPEN) {
			COM_IS_OPEN = OpenComm(DEVICE3, 921600, 8, ONESTOPBIT, NOPARITY);//尝试打开串口
			open_count++;
			if (open_count > 10)
				break;
		}
	if (key_to_room == 4)
		while (!COM_IS_OPEN) {
			COM_IS_OPEN = OpenComm(DEVICE4, 921600, 8, ONESTOPBIT, NOPARITY);//尝试打开串口
			open_count++;
			if (open_count > 10)
				break;
		}
	if (COM_IS_OPEN == 0)
	{
		MessageBox(_T("串口打开失败，请重启log窗口"), _T("提示"), MB_ICONINFORMATION);
		return FALSE;
	}
	COMISOK = COM_IS_OPEN;
		
	
	return TRUE;
}





void CDevicelog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	if (this->IsDlgButtonChecked(IDC_READ))
	{
		if (COMISOK)
		{

			DWORD dwBytesRead = 100;

			ClearCommError(hCom, &dwErrorFlags, &ComStat);

			dwBytesRead = min(dwBytesRead, (DWORD)ComStat.cbInQue);

			if (dwBytesRead)
				ReadComm();

		}
	}
	
	if (nIDEvent == 0)
	{
		this->GetDlgItem(IDC_LOG_STATE)->SetWindowText(LOG_STATE);
	}
	CDialogEx::OnTimer(nIDEvent);
}

BOOL CDevicelog::OpenComm(CString Device,int nBaud, int nData, int nStop, int nCal) {
	int nport = -1;
	CString COM = TEXT("COM");
	nport = MTGetPortFromVidPid(Device);
	if (nport == -1)
	{
		LOG_STATE = TEXT("状态：无设备");
		return FALSE;
	}
	else {
		COM.Format(TEXT("\\\\.\\COM%d"), nport);
		hCom = CreateFile(COM, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);//打开串口
		if (hCom == (HANDLE)-1)
		{
			LOG_STATE = TEXT("状态：串口打开失败");
			CloseHandle(hCom);
			return FALSE;
		}
		else
		{
			COMMTIMEOUTS TimeOuts;
			//设定读超时
			TimeOuts.ReadIntervalTimeout = MAXDWORD;
			TimeOuts.ReadTotalTimeoutMultiplier = 0;
			TimeOuts.ReadTotalTimeoutConstant = 0;
			//在读一次输入缓冲区的内容后读操作就立即返回，
			//而不管是否读入了要求的字符。


			//设定写超时
			TimeOuts.WriteTotalTimeoutMultiplier = 100;
			TimeOuts.WriteTotalTimeoutConstant = 500;
			SetCommTimeouts(hCom, &TimeOuts); //设置超时
			SetupComm(hCom, 1024, 1024); //输入缓冲区和输出缓冲区的大小都是1024
			DCB wdcb;
			GetCommState(hCom, &wdcb);
			wdcb.BaudRate = nBaud;//波特率
			wdcb.ByteSize = nData;   
			wdcb.StopBits = nStop;
			wdcb.Parity = nCal;  
			SetCommState(hCom, &wdcb);
			PurgeComm(hCom, PURGE_TXCLEAR | PURGE_RXCLEAR);
			if (SetCommState(hCom, &wdcb) == false) {
				CloseHandle(hCom);
				LOG_STATE = TEXT("状态：串口设置失败");
				return FALSE;
			}
			LOG_STATE = TEXT("状态：串口打开成功");
		}
	}
	return TRUE;
}

BOOL CDevicelog::ReadComm()
{
	CString strTemp;
	OVERLAPPED m_osRead;
	memset(&m_osRead, 0, sizeof(OVERLAPPED));
	m_osRead.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	char lpInBuffer[1025];
	DWORD dwBytesRead = 1024;
	BOOL bReadStatus;
	bReadStatus = ReadFile(hCom, lpInBuffer, dwBytesRead, &dwBytesRead, &m_osRead);
	if (!bReadStatus) //如果ReadFile函数返回FALSE 
	{
		if (GetLastError() == ERROR_IO_PENDING) //GetLastError()函数返回ERROR_IO_PENDING,表明串口正在进行读操作 
		{
			WaitForSingleObject(m_osRead.hEvent, 2000); //使用WaitForSingleObject函数等待，直到读操作完成或延时已达到2000ms 
			//当串口读操作进行完毕后，m_osRead的hEvent事件会变为有信号 
			PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
			return dwBytesRead;
		}
		return 0;
	}
	lpInBuffer[dwBytesRead] = '\0';
	strTemp = lpInBuffer;
	for (int i = 0; i < 1024; i++)//替换换行符，可以考虑把1024改小

	{

		int index = strTemp.Find(L"\n");
		if (index == -1)
			continue;
		else
		{
			strTemp.Replace(L"\n", L"\r\n");//\r\n才是换行
		}
	}
	SYSTEMTIME st;
	CString strTime;
	GetLocalTime(&st);
	strTime.Format(_T("%4d-%2d-%2d,%2d:%2d:%2d"), st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
	DataRX += strTime;
	DataRX += " received";
	DataRX += "\r\n";
	DataRX += strTemp;
	GetDlgItem(IDC_LOG_DATA)->SetWindowText(DataRX);
	SendDlgItemMessage(IDC_LOG_DATA, WM_VSCROLL, SB_BOTTOM, 0);//滚动条在底部
	//OnCheckHexrx();
	return 1;
}





void CDevicelog::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
		DataRX = "";
		if (COMISOK == TRUE)
		{
			COMISOK = FALSE;
			CloseHandle(hCom);
		}
		
	CDialogEx::OnClose();
}




void CDevicelog::OnBnClickedBtnsend()
{
	// TODO: 在此添加控件通知处理程序代码
	CString strOut;
	if (COMISOK == FALSE)
	{
		MessageBox(_T("请先打开串口"), _T("提示"), MB_ICONINFORMATION);
		return;//return 0;
	}
	BOOL bWriteStat;
	UpdateData(TRUE);
	DWORD dwBytesWritten;
	DWORD dwBytes2Write = 1024;
	OVERLAPPED m_osWrite;
	memset(&m_osWrite, 0, sizeof(OVERLAPPED));
	m_osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	COMSTAT ComStat;
	DWORD dwErrorFlags;
	GetDlgItem(IDC_TEXT2SEND)->GetWindowText(strOut);
	dwBytes2Write = strOut.GetLength();
	int nBytes = WideCharToMultiByte(CP_ACP, 0, strOut, dwBytes2Write, NULL, 0, NULL, NULL);
	char* lpOutBuffer = new char[nBytes + 1];
	memset(lpOutBuffer, 0, dwBytes2Write + 1);
	WideCharToMultiByte(CP_OEMCP, 0, strOut, dwBytes2Write, lpOutBuffer, nBytes, NULL, NULL);
	lpOutBuffer[nBytes] = 0;

	if (dwBytes2Write == 0)
	{
		MessageBox(_T("请在发送区内输入要发送的内容"), _T("提示"), MB_ICONINFORMATION);
		return;
	}
	ClearCommError(hCom, &dwErrorFlags, &ComStat);
	bWriteStat = WriteFile(hCom, lpOutBuffer, dwBytes2Write, &dwBytesWritten, &m_osWrite);
	if (!bWriteStat)
	{
		if (GetLastError() == ERROR_IO_PENDING)
		{
			WaitForSingleObject(m_osWrite.hEvent, 1000);
		}
		return;
	}
	PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
	return;
}
