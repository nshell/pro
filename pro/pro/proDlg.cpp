
// proDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "pro.h"
#include "proDlg.h"
#include "afxdialogex.h"
#include "display_info.h"

#include <shlwapi.h>
#define APP_LENGTH 1200*rate
#define APP_WIDTH 750*rate
#define CURRENT_DPI 96
#define APP_XPOSITION (x_pos / 2)-APP_LENGTH/2
#define APP_YPOSITION (y_pos / 2)-APP_WIDTH/2

#define PROGRESS_XPOSITION 250*rate
#define PROGRESS_YPOSITION 40*rate
#define PROGRESS_LENGTH    900*rate
#define PROGRESS_WIDTH		24*rate
#define PROGRESS_UP_DOWN_GAP 44*rate


#define STATIC_XPOSITION   181*rate
#define STATIC_YPOSITION   40*rate
#define STATIC_LENGTH      64*rate
#define STATIC_WIDTH	   21*rate
#define STATIC_UP_DOWN_GAP 44*rate
#define STATE_XPOSITIN     5*rate
#define STATE_LENGTH       150*rate
#define STATE_WIDTH        15*rate
#define BLABEL_XPOSITION   125*rate
#define BLABEL_YPOSITION   230*rate
#define BLABEL_L_R_GAP   300*rate
#define BLABEL_U_P_GAP   30*rate
#define BLABEL_LENGTH   50*rate
#define OTALABEL_LENGTH   60*rate
#define BTLABEL_XPOSITION 35*rate
#define BINLABEL_XPOSITIOPN 130*rate
#define OTALABEL_XPOSITIOPN 120*rate
#define BINLABEL_YPOSITIOPN 430*rate
#define BINLABEL_GAP 30*rate

#define PRO_XPOSITION 25*rate
#define POW_XPOSITION 325*rate
#define RTS_XPOSITION 625*rate
#define FAC_XPOSITION 925*rate
#define BUTTON_YPOSITION 530*rate
#define BUTTON_LENGTH 250*rate
#define BUTTON_WIDTH 150*rate
//#define verso_m

#pragma comment(lib,"Shlwapi.lib")
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框
display_info INFO_STATE;

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
public:
	CStatic logo_about;
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{

}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PICTURE, logo_about);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CproDlg 对话框



CproDlg::CproDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CproDlg::IDD, pParent)
{
	
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

CproDlg::~CproDlg()
{

	if (NULL != m_hDevNotify)
	{
		UnregisterDeviceNotification(m_hDevNotify);
		m_hDevNotify = NULL;
	}

}

void CproDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS1, prog);
	DDX_Control(pDX, IDC_BTADDR1, Bt_addr1);
	DDX_Control(pDX, IDC_BTADDR2, Bt_addr2);
	DDX_Control(pDX, IDC_BTADDR3, Bt_addr3);
	DDX_Control(pDX, IDC_BTADDR4, Bt_addr4);
	DDX_Control(pDX, IDC_BLEADDR1, Ble_addr1);
	DDX_Control(pDX, IDC_BLEADDR2, Ble_addr2);
	DDX_Control(pDX, IDC_BLEADDR3, Ble_addr3);
	DDX_Control(pDX, IDC_BLEADDR4, Ble_addr4);
	DDX_Control(pDX, IDC_PROGRESS2, prog2);
	DDX_Control(pDX, IDC_PROGRESS3, prog3);
	DDX_Control(pDX, IDC_PROGRESS4, prog4);
}

BEGIN_MESSAGE_MAP(CproDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_PRO, &CproDlg::OnBnClickedPro)
	ON_WM_CTLCOLOR()
	ON_EN_CHANGE(IDC_BTADDR1, &CproDlg::OnEnChangeBtaddr0)
	ON_EN_CHANGE(IDC_BTADDR2, &CproDlg::OnEnChangeBtaddr0)
	ON_EN_CHANGE(IDC_BTADDR3, &CproDlg::OnEnChangeBtaddr0)
	ON_EN_CHANGE(IDC_BTADDR4, &CproDlg::OnEnChangeBtaddr0)
	ON_EN_CHANGE(IDC_BLEADDR1, &CproDlg::OnEnChangeBtaddr0)
	ON_EN_CHANGE(IDC_BLEADDR2, &CproDlg::OnEnChangeBtaddr0)
	ON_EN_CHANGE(IDC_BLEADDR3, &CproDlg::OnEnChangeBtaddr0)
	ON_EN_CHANGE(IDC_BLEADDR4, &CproDlg::OnEnChangeBtaddr0)
	ON_EN_CHANGE(IDC_BLE1NAME, &CproDlg::OnEnChangename)
	ON_EN_CHANGE(IDC_BLE2NAME, &CproDlg::OnEnChangename)
	ON_EN_CHANGE(IDC_BLE3NAME, &CproDlg::OnEnChangename)
	ON_EN_CHANGE(IDC_BLE4NAME, &CproDlg::OnEnChangename)
	ON_EN_CHANGE(IDC_BT1NAME, &CproDlg::OnEnChangename)
	ON_EN_CHANGE(IDC_BT2NAME, &CproDlg::OnEnChangename)
	ON_EN_CHANGE(IDC_BT3NAME, &CproDlg::OnEnChangename)
	ON_EN_CHANGE(IDC_BT4NAME, &CproDlg::OnEnChangename)
	ON_BN_CLICKED(IDC_POW, &CproDlg::OnBnClickedPow)
	ON_BN_CLICKED(IDC_CHOOSEBIN, &CproDlg::OnBnClickedChoosebin)
	ON_BN_CLICKED(IDC_CHOOSEOTA, &CproDlg::OnBnClickedChooseota)
	ON_BN_CLICKED(IDC_RTS, &CproDlg::OnBnClickedRts)
	ON_BN_CLICKED(IDC_FAC, &CproDlg::OnBnClickedFac)
	ON_COMMAND(ID_ACCELERATOR_CTRL_D, &CproDlg::OnAcceleratorCtrlD)
	ON_BN_CLICKED(IDC_ERASE, &CproDlg::OnBnClickedErase)
	ON_WM_TIMER()
	ON_WM_DEVICECHANGE()
	ON_BN_CLICKED(IDC_LOG1, &CproDlg::OnBnClickedLog1)
	ON_BN_CLICKED(IDC_LOG2, &CproDlg::OnBnClickedLog2)
	ON_BN_CLICKED(IDC_LOG3, &CproDlg::OnBnClickedLog3)
	ON_BN_CLICKED(IDC_LOG4, &CproDlg::OnBnClickedLog4)
END_MESSAGE_MAP()


// CproDlg 消息处理程序
char dangleAddr[6];

BOOL CproDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
	DEV_BROADCAST_DEVICEINTERFACE dbdi;
	memset(&dbdi, 0, sizeof(dbdi));
	dbdi.dbcc_size = sizeof(dbdi);
	dbdi.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	m_hDevNotify = RegisterDeviceNotification(this->GetSafeHwnd(), &dbdi, DEVICE_NOTIFY_WINDOW_HANDLE | DEVICE_NOTIFY_ALL_INTERFACE_CLASSES);

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标
	 m_hAccel = ::LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_ACCELERATOR1));

	
	
	 CRect rect;
	 GetClientRect(&rect);     //取客户区大小  
	 old.x = rect.right - rect.left;
	 old.y = rect.bottom - rect.top;
	 int cx = GetSystemMetrics(SM_CXFULLSCREEN);
	 int cy = GetSystemMetrics(SM_CYFULLSCREEN);
	 CRect rt;
	 SystemParametersInfo(SPI_GETWORKAREA, 0, &rt, 0);
	 cy = rt.bottom;
	// MoveWindow(0, 0, cx/2, cy/2);
	 DPI_SCALE = 0;
	 CDC* screen = GetDC();
	 int dpiX = screen->GetDeviceCaps(LOGPIXELSX);
	 int dpiY = screen->GetDeviceCaps(LOGPIXELSY);
	 ReleaseDC(screen);
	 DPI_SCALE = dpiX;
	 rate = (float)DPI_SCALE / (float)CURRENT_DPI;

	CString path;
	GetModuleFileName(NULL, path.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
	path.ReleaseBuffer();
	int pos = path.ReverseFind('\\');
	path = path.Left(pos);
	TCHAR CONFIG_STR[MAX_PATH];
	BOOL FLAG = PathFileExists(path + TEXT("\\config.ini"));

	if (!FLAG)
	{
		WritePrivateProfileString(_T("addrinfo"), _T("Ble_addr1"), TEXT("12:34:56:78:9A:BC"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("addrinfo"), _T("Ble_addr2"), TEXT("12:34:56:78:9A:BC"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("addrinfo"), _T("Ble_addr3"), TEXT("12:34:56:78:9A:BC"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("addrinfo"), _T("Ble_addr4"), TEXT("12:34:56:78:9A:BC"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("addrinfo"), _T("Bt_addr1"), TEXT("12:34:56:78:9A:BC"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("addrinfo"), _T("Bt_addr2"), TEXT("12:34:56:78:9A:BC"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("addrinfo"), _T("Bt_addr3"), TEXT("12:34:56:78:9A:BC"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("addrinfo"), _T("Bt_addr4"), TEXT("12:34:56:78:9A:BC"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("nameinfo"), _T("Ble_name1"), TEXT("BLE_T"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("nameinfo"), _T("Ble_name2"), TEXT("BLE_T"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("nameinfo"), _T("Ble_name3"), TEXT("BLE_T"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("nameinfo"), _T("Ble_name4"), TEXT("BLE_T"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("nameinfo"), _T("Bt_name1"), TEXT("BT_T"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("nameinfo"), _T("Bt_name2"), TEXT("BT_T"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("nameinfo"), _T("Bt_name3"), TEXT("BT_T"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("nameinfo"), _T("Bt_name4"), TEXT("BT_T"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("pathinfo"), _T("PATHBIN"), TEXT("C:\\Users\\nshel\\Documents\\bes_tws_verso\\out\\best2300_tws\\best2300_tws.bin"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("pathinfo"), _T("PATHOTA"), TEXT("C:\\Users\\nshel\\Desktop\\ota_boot_dual_boot_4e4e120a22614ce3f549651938368082881750db.bin"), path + TEXT("\\config.ini"));
		WritePrivateProfileString(_T("checkinfo"), _T("erase"), TEXT("ERASE_TRUE"), path + TEXT("\\config.ini"));
	}
	
	

    GetPrivateProfileString(_T("addrinfo"), _T("Bt_addr4"), _T("Bt_addr4"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->Bt_addr4.SetWindowText(CONFIG_STR);
	GetPrivateProfileString(_T("addrinfo"), _T("Bt_addr3"), _T("Bt_addr3"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->Bt_addr3.SetWindowText(CONFIG_STR);
	GetPrivateProfileString(_T("addrinfo"), _T("Bt_addr2"), _T("Bt_addr2"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->Bt_addr2.SetWindowText(CONFIG_STR);
	GetPrivateProfileString(_T("addrinfo"), _T("Bt_addr1"), _T("Bt_addr1"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->Bt_addr1.SetWindowText(CONFIG_STR);
	GetPrivateProfileString(_T("addrinfo"), _T("Ble_addr4"), _T("Ble_addr4"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->Ble_addr4.SetWindowText(CONFIG_STR);
	GetPrivateProfileString(_T("addrinfo"), _T("Ble_addr3"), _T("Ble_addr3"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->Ble_addr3.SetWindowText(CONFIG_STR);
	GetPrivateProfileString(_T("addrinfo"), _T("Ble_addr2"), _T("Ble_addr2"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->Ble_addr2.SetWindowText(CONFIG_STR);
	GetPrivateProfileString(_T("addrinfo"), _T("Ble_addr1"), _T("Ble_addr1"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->Ble_addr1.SetWindowText(CONFIG_STR);
	GetPrivateProfileString(_T("pathinfo"), _T("PATHBIN"), _T("PATHBIN"), CONFIG_STR, 200, path + TEXT("\\config.ini"));
	this->GetDlgItem(IDC_PATHBIN)->SetWindowTextW(CONFIG_STR);
	GetPrivateProfileString(_T("pathinfo"), _T("PATHOTA"), _T("PATHOTA"), CONFIG_STR, 200, path + TEXT("\\config.ini"));
	this->GetDlgItem(IDC_PATHOTA)->SetWindowTextW(CONFIG_STR);
	GetPrivateProfileString(_T("nameinfo"), _T("Ble_name1"), _T("Ble_name"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->GetDlgItem(IDC_BLE1NAME)->SetWindowTextW(CONFIG_STR);
	GetPrivateProfileString(_T("nameinfo"), _T("Ble_name2"), _T("Ble_name"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->GetDlgItem(IDC_BLE2NAME)->SetWindowTextW(CONFIG_STR);
	GetPrivateProfileString(_T("nameinfo"), _T("Ble_name3"), _T("Ble_name"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->GetDlgItem(IDC_BLE3NAME)->SetWindowTextW(CONFIG_STR);
	GetPrivateProfileString(_T("nameinfo"), _T("Ble_name4"), _T("Ble_name"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->GetDlgItem(IDC_BLE4NAME)->SetWindowTextW(CONFIG_STR);
	GetPrivateProfileString(_T("nameinfo"), _T("Bt_name1"), _T("Bt_name"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->GetDlgItem(IDC_BT1NAME)->SetWindowTextW(CONFIG_STR);
	GetPrivateProfileString(_T("nameinfo"), _T("Bt_name2"), _T("Bt_name"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	this->GetDlgItem(IDC_BT2NAME)->SetWindowTextW(CONFIG_STR);
	GetPrivateProfileString(_T("nameinfo"), _T("Bt_name3"), _T("Bt_name"), CONFIG_STR, 50, path + TEXT("\\config.ini")); 
	this->GetDlgItem(IDC_BT3NAME)->SetWindowTextW(CONFIG_STR);
	GetPrivateProfileString(_T("nameinfo"), _T("Bt_name4"), _T("Bt_name"), CONFIG_STR, 50, path + TEXT("\\config.ini")); 
	this->GetDlgItem(IDC_BT4NAME)->SetWindowTextW(CONFIG_STR);
	GetPrivateProfileString(_T("checkinfo"), _T("erase"), _T("ERASE"), CONFIG_STR, 50, path + TEXT("\\config.ini"));
	CString s(CONFIG_STR);
	if (s == TEXT("ERASE_TRUE")) this->CheckDlgButton(IDC_ERASE,1);
	if (s == TEXT("ERASE_FALSE")) this->CheckDlgButton(IDC_ERASE, 0);
	for (int i = IDC_BTLABE; i < IDC_LOG4 + 1; i++)
		this->GetDlgItem(i)->ShowWindow(false);
	//APP SCREEN POSITION
	int x_pos = GetSystemMetrics(SM_CXFULLSCREEN);
	int y_pos = GetSystemMetrics(SM_CYFULLSCREEN);
	this->SetWindowPos(NULL, APP_XPOSITION, APP_YPOSITION, APP_LENGTH, APP_WIDTH, SWP_NOZORDER);

	// progress control position
	this->GetDlgItem(IDC_PROGRESS1)->SetWindowPos(NULL, PROGRESS_XPOSITION, PROGRESS_YPOSITION, PROGRESS_LENGTH, PROGRESS_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_PROGRESS2)->SetWindowPos(NULL, PROGRESS_XPOSITION, (PROGRESS_YPOSITION+ PROGRESS_UP_DOWN_GAP)  , PROGRESS_LENGTH, PROGRESS_WIDTH , SWP_NOZORDER);
	this->GetDlgItem(IDC_PROGRESS3)->SetWindowPos(NULL, PROGRESS_XPOSITION, (PROGRESS_YPOSITION + PROGRESS_UP_DOWN_GAP*2), PROGRESS_LENGTH , PROGRESS_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_PROGRESS4)->SetWindowPos(NULL, PROGRESS_XPOSITION, (PROGRESS_YPOSITION + PROGRESS_UP_DOWN_GAP * 3), PROGRESS_LENGTH, PROGRESS_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_STATIC1)->SetWindowPos(NULL, STATIC_XPOSITION, STATIC_YPOSITION, STATIC_LENGTH, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_STATIC2)->SetWindowPos(NULL, STATIC_XPOSITION, STATIC_YPOSITION + STATIC_UP_DOWN_GAP, STATIC_LENGTH, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_STATIC3)->SetWindowPos(NULL, STATIC_XPOSITION, STATIC_YPOSITION + STATIC_UP_DOWN_GAP*2, STATIC_LENGTH, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_STATIC4)->SetWindowPos(NULL, STATIC_XPOSITION, STATIC_YPOSITION + STATIC_UP_DOWN_GAP*3, STATIC_LENGTH, STATIC_WIDTH, SWP_NOZORDER);

	//state label position 
	this->GetDlgItem(IDC_STATE1)->SetWindowPos(NULL, STATE_XPOSITIN, STATIC_YPOSITION, STATE_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_STATE2)->SetWindowPos(NULL, STATE_XPOSITIN, STATIC_YPOSITION + STATIC_UP_DOWN_GAP, STATE_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_STATE3)->SetWindowPos(NULL, STATE_XPOSITIN, STATIC_YPOSITION + STATIC_UP_DOWN_GAP * 2, STATE_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_STATE4)->SetWindowPos(NULL, STATE_XPOSITIN, STATIC_YPOSITION + STATIC_UP_DOWN_GAP * 3, STATE_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	//equipment label position 
	this->GetDlgItem(IDC_BTLABE3)->SetWindowPos(NULL, BLABEL_XPOSITION, BLABEL_YPOSITION, STATE_LENGTH/2, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE6)->SetWindowPos(NULL, BLABEL_XPOSITION+ BLABEL_L_R_GAP, BLABEL_YPOSITION, STATE_LENGTH / 2, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE9)->SetWindowPos(NULL, BLABEL_XPOSITION + BLABEL_L_R_GAP*2, BLABEL_YPOSITION, STATE_LENGTH / 2, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE12)->SetWindowPos(NULL, BLABEL_XPOSITION + BLABEL_L_R_GAP*3, BLABEL_YPOSITION, STATE_LENGTH / 2, STATE_WIDTH, SWP_NOZORDER);
	// device No.1 bt ble address and name position
	this->GetDlgItem(IDC_BTLABE)->SetWindowPos(NULL, BTLABEL_XPOSITION, BLABEL_YPOSITION + BLABEL_U_P_GAP, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE2)->SetWindowPos(NULL, BTLABEL_XPOSITION, BLABEL_YPOSITION + BLABEL_U_P_GAP*2, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE14)->SetWindowPos(NULL, BTLABEL_XPOSITION, BLABEL_YPOSITION + BLABEL_U_P_GAP*3, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE15)->SetWindowPos(NULL, BTLABEL_XPOSITION, BLABEL_YPOSITION + BLABEL_U_P_GAP*4, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTADDR1)->SetWindowPos(NULL, BTLABEL_XPOSITION+ BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP, STATE_WIDTH*10, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BLEADDR1)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 2, STATE_WIDTH * 10, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BT1NAME)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 3, STATE_WIDTH * 10, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BLE1NAME)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 4, STATE_WIDTH * 10, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_LOG1)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 5, STATE_WIDTH * 10, STATIC_WIDTH, SWP_NOZORDER);
	//device No.2 bt ble address and name position
	this->GetDlgItem(IDC_BTLABE4)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP, BLABEL_YPOSITION + BLABEL_U_P_GAP, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE5)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP, BLABEL_YPOSITION + BLABEL_U_P_GAP*2, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE16)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP, BLABEL_YPOSITION + BLABEL_U_P_GAP * 3, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE17)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP, BLABEL_YPOSITION + BLABEL_U_P_GAP * 4, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTADDR2)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP+ BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP , BLABEL_LENGTH*3, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BLEADDR2)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 2, BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BT2NAME)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 3, BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BLE2NAME)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 4, BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_LOG2)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 5, BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);

	//device No.3 bt ble address and name position
	this->GetDlgItem(IDC_BTLABE7)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP*2, BLABEL_YPOSITION + BLABEL_U_P_GAP, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE8)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 2, BLABEL_YPOSITION + BLABEL_U_P_GAP * 2, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE18)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 2, BLABEL_YPOSITION + BLABEL_U_P_GAP * 3, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE19)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 2, BLABEL_YPOSITION + BLABEL_U_P_GAP * 4, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTADDR3)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP*2 + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP, BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BLEADDR3)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 2 + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 2, BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BT3NAME)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 2 + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 3, BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BLE3NAME)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 2 + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 4, BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_LOG3)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 2 + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 5, BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);

	//device No.4 bt ble address and name position
	this->GetDlgItem(IDC_BTLABE10)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 3, BLABEL_YPOSITION + BLABEL_U_P_GAP, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE11)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 3, BLABEL_YPOSITION + BLABEL_U_P_GAP*2, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE20)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 3, BLABEL_YPOSITION + BLABEL_U_P_GAP * 3, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTLABE21)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 3, BLABEL_YPOSITION + BLABEL_U_P_GAP * 4, BLABEL_LENGTH, STATE_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BTADDR4)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 3 + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP , BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BLEADDR4)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 3 + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 2, BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BT4NAME)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 3 + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 3, BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_BLE4NAME)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 3 + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 4, BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_LOG4)->SetWindowPos(NULL, BTLABEL_XPOSITION + BLABEL_L_R_GAP * 3 + BLABEL_LENGTH, BLABEL_YPOSITION + BLABEL_U_P_GAP * 5, BLABEL_LENGTH * 3, STATIC_WIDTH, SWP_NOZORDER);


	this->GetDlgItem(IDC_BINLABE)->SetWindowPos(NULL, BINLABEL_XPOSITIOPN, BINLABEL_YPOSITIOPN, BLABEL_LENGTH, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_OTALABE)->SetWindowPos(NULL, OTALABEL_XPOSITIOPN, BINLABEL_YPOSITIOPN + BINLABEL_GAP, OTALABEL_LENGTH, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_PATHBIN)->SetWindowPos(NULL, BINLABEL_XPOSITIOPN + BLABEL_LENGTH, BINLABEL_YPOSITIOPN, OTALABEL_LENGTH*10, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_PATHOTA)->SetWindowPos(NULL, BINLABEL_XPOSITIOPN + BLABEL_LENGTH, BINLABEL_YPOSITIOPN + BINLABEL_GAP, OTALABEL_LENGTH * 10, STATIC_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_CHOOSEBIN)->SetWindowPos(NULL, 790*rate, 427*rate, STATE_LENGTH, PROGRESS_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_CHOOSEOTA)->SetWindowPos(NULL, 790 * rate, 457*rate, STATE_LENGTH, PROGRESS_WIDTH, SWP_NOZORDER);

	this->GetDlgItem(IDC_ERASE)->SetWindowPos(NULL,980*rate, 445*rate, 150*rate, 25*rate, SWP_NOZORDER);

	this->GetDlgItem(IDC_PRO)->SetWindowText(TEXT("烧录"));
	this->GetDlgItem(IDC_POW)->SetWindowText(TEXT("开机"));
	this->GetDlgItem(IDC_RTS)->SetWindowText(TEXT("参考模式"));
	this->GetDlgItem(IDC_FAC)->SetWindowText(TEXT("出厂设置"));
	this->GetDlgItem(IDC_PRO)->SetWindowPos(NULL, PRO_XPOSITION, BUTTON_YPOSITION, BUTTON_LENGTH, BUTTON_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_POW)->SetWindowPos(NULL, POW_XPOSITION, BUTTON_YPOSITION, BUTTON_LENGTH, BUTTON_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_RTS)->SetWindowPos(NULL, RTS_XPOSITION, BUTTON_YPOSITION, BUTTON_LENGTH, BUTTON_WIDTH, SWP_NOZORDER);
	this->GetDlgItem(IDC_FAC)->SetWindowPos(NULL, FAC_XPOSITION, BUTTON_YPOSITION, BUTTON_LENGTH, BUTTON_WIDTH, SWP_NOZORDER);
	CFont* font = new	CFont;
	CFont* font1 = new	CFont;
	font->CreateFont(100, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, TEXT("华文楷体"));
	this->GetDlgItem(IDC_PRO)->SetFont(font);
	this->GetDlgItem(IDC_POW)->SetFont(font);
	font1->CreateFont(50, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, TEXT("华文楷体"));
	this->GetDlgItem(IDC_RTS)->SetFont(font1);
	this->GetDlgItem(IDC_FAC)->SetFont(font1);
	


	/*GPIO_SetCBUSConfig(DEVICE1, CBUS0 | CBUS1 | CBUS2 | CBUS3, 0);
	GPIO_SetCBUSConfig(DEVICE2, CBUS0 | CBUS1 | CBUS2 | CBUS3, 0);
	GPIO_SetCBUSConfig(DEVICE3, CBUS0 | CBUS1 | CBUS2 | CBUS3, 0);
	GPIO_SetCBUSConfig(DEVICE4, CBUS0 | CBUS1 | CBUS2 | CBUS3, 0);
	GPIO_SetCBUSConfig(SWITCH, CBUS0 | CBUS1 | CBUS2 | CBUS3, 0);*/

	GPIO_SetDxConfig(SWITCH, D0 | D1 | D2 | D3 | D4 | D5 | D6 | D7, 0);


	dangleAddr[5] = 0x33;//high address
	dangleAddr[4] = 0x33;
	dangleAddr[3] = 0x33;
	dangleAddr[2] = 0x33;
	dangleAddr[1] = 0x33;
	dangleAddr[0] = 0x33;
	this->prog.SetRange(0, 97);
	this->prog2.SetRange(0, 97);
	this->prog3.SetRange(0, 97);
	this->prog4.SetRange(0, 97);
	 INFO_STATE.EnableAllBtn();
	 int i;
	 for (i = 0; i < 4; i++)
	 {
		 this->GetDlgItem(IDC_STATE1+i)->SetWindowText(INFO_STATE.Device_state[i]);
		 this->GetDlgItem(IDC_BTADDR1 + i)->GetWindowText(INFO_STATE.Bt_Addr[i]);
		 this->GetDlgItem(IDC_BLEADDR1 + i)->GetWindowText(INFO_STATE.Ble_Addr[i]);
		 this->GetDlgItem(IDC_BLE1NAME + i)->GetWindowText(INFO_STATE.Ble_Name[i]);
		 this->GetDlgItem(IDC_BT1NAME + i)->GetWindowText(INFO_STATE.Bt_Name[i]);
	 }
	 this->GetDlgItem(IDC_PATHBIN)->GetWindowText(INFO_STATE.Bin_path);
	 this->GetDlgItem(IDC_PATHOTA)->GetWindowText(INFO_STATE.Ota_path);
	 INFO_STATE.Is_erase[0]=this->IsDlgButtonChecked(IDC_ERASE);

	SetTimer(0, 100, NULL);	//100ms

	//扫码枪线程一直保持打开
	LPVOID                 pParam = NULL;
	int                    nPriority = THREAD_PRIORITY_NORMAL;//默认为THREAD_PRIORITY_NORMAL
	UINT                   nStackSize = 0;//与创建它的线程堆栈大小相同
	DWORD                  dwCreateFlags = 0;//创建后立即执行
	LPSECURITY_ATTRIBUTES  lpSecurityAttrs = NULL;//与创建它的线程安全属性相同
	Channe6_Thread = AfxBeginThread((AFX_THREADPROC)Channe6_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CproDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。
int MODE= DEFAULT_MODE;

void CproDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}




//当用户拖动最小化窗口时系统调用此函数取得光标显示。
HCURSOR CproDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


int burn_complete_num = 0;
int burn_click_flag = 0;
BOOL burn_stop_flag = FALSE;
void CproDlg::OnBnClickedPro()
{
	if (burn_click_flag == 0) {	//首次点击执行烧录功能
		LPVOID                 pParam = NULL;
		int                    nPriority = THREAD_PRIORITY_NORMAL;//默认为THREAD_PRIORITY_NORMAL
		UINT                   nStackSize = 0;//与创建它的线程堆栈大小相同
		DWORD                  dwCreateFlags = 0;//创建后立即执行
		LPSECURITY_ATTRIBUTES  lpSecurityAttrs = NULL;//与创建它的线程安全属性相同

		INFO_STATE.Is_Burn_Enable = FALSE;
		INFO_STATE.Is_Fac_Enable = FALSE;
		INFO_STATE.Is_Pow_Enable = FALSE;
		INFO_STATE.Is_RTS_Enable = FALSE;
		MODE = PRO_MODE;

		Channel_Thread = AfxBeginThread((AFX_THREADPROC)Channel_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
		Channe2_Thread = AfxBeginThread((AFX_THREADPROC)Channe2_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
		Channe3_Thread = AfxBeginThread((AFX_THREADPROC)Channe3_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
		Channe4_Thread = AfxBeginThread((AFX_THREADPROC)Channe4_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
	}
	else {	//再次点击执行暂停烧录功能
		burn_stop_flag = TRUE;
	}
	INFO_STATE.Is_Burn_Enable = TRUE;
	burn_click_flag++;

}


void CproDlg::OnBnClickedPow()
{

	if (Developer_Mode == 0) {
		LPVOID                 pParam = NULL;
		int                    nPriority = THREAD_PRIORITY_HIGHEST;//默认为THREAD_PRIORITY_NORMAL
		UINT                   nStackSize = 0;//与创建它的线程堆栈大小相同
		DWORD                  dwCreateFlags = 0;//创建后立即执行
		LPSECURITY_ATTRIBUTES  lpSecurityAttrs = NULL;//与创建它的线程安全属性相同
		INFO_STATE.Is_Burn_Enable = FALSE;
		INFO_STATE.Is_Fac_Enable = FALSE;
		INFO_STATE.Is_Pow_Enable = FALSE;
		INFO_STATE.Is_RTS_Enable = FALSE;
		MODE = POW_MODE;
		Channel_Thread = AfxBeginThread((AFX_THREADPROC)Channel_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
		Channe2_Thread = AfxBeginThread((AFX_THREADPROC)Channe2_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
		Channe3_Thread = AfxBeginThread((AFX_THREADPROC)Channe3_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
		Channe4_Thread = AfxBeginThread((AFX_THREADPROC)Channe4_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
	}

	if (Developer_Mode == 1)
	{
		int i;
		for (i = 0; i < 4; i++)
		{
			INFO_STATE.Device_state[i] = TEXT("状态：开始开机");
		}

		BOOL comstate = true;
		comstate = SETCOMOUTPUT(DEVICE1, DTR, 100) & SETCOMOUTPUT(DEVICE2, DTR, 100) &
			SETCOMOUTPUT(DEVICE3, DTR, 100) & SETCOMOUTPUT(DEVICE4, DTR, 100);
		if (comstate == false)
		{
			for (i = 0; i < 4; i++)
			{
				INFO_STATE.Device_state[i] = TEXT("状态：开机失败");
			}
		}
		if (comstate == true)
		{
			for (i = 0; i < 4; i++)
			{
				INFO_STATE.Device_state[i] = TEXT("状态：开机成功");
			}
		}

		INFO_STATE.Is_Burn_Enable = TRUE;
		INFO_STATE.Is_Fac_Enable = TRUE;
		INFO_STATE.Is_Pow_Enable = TRUE;
		INFO_STATE.Is_RTS_Enable = TRUE;
	}

}

extern BOOL device1_flag;
extern BOOL device4_flag;

UINT Channel_WorkForce(LPVOID pParam)
{
	CString path_app_bin ;
	CString path_factory_bin;
	CString path_ota_bin ;
	CString path_programmer_bin;
	INFO_STATE.DisableAllBtn();

	switch (MODE) {
	case PRO_MODE:
		Burn_Progress(DEVICE1, path_app_bin, path_factory_bin, path_programmer_bin, path_ota_bin);
		break;
	case POW_MODE:Poweron(DEVICE1);
	break;
	case RTS_MODE:RTS_Process(DEVICE1);
	break;
	case FAC_MODE:FAS_Process(DEVICE1);
	break;
	default:     break;
	}
	burn_complete_num = burn_complete_num | 0x01;
	if (burn_complete_num == 0x0f)
	{
		device1_flag = device4_flag = FALSE;

		INFO_STATE.EnableAllBtn();
		burn_complete_num = 0;
		burn_click_flag = 0;
		burn_stop_flag = FALSE;
	}
	return 1;
}
UINT Channe2_WorkForce(LPVOID pParam)
{
	CString path_app_bin;
	CString path_factory_bin;
	CString path_ota_bin;
	CString path_programmer_bin;
	switch (MODE) {
	case PRO_MODE:
		Burn_Progress(DEVICE2, path_app_bin, path_factory_bin, path_programmer_bin, path_ota_bin);
	break;
	case POW_MODE:Poweron(DEVICE2);

		break;
	case RTS_MODE:RTS_Process(DEVICE2); 
	break;
	case FAC_MODE:FAS_Process(DEVICE2);break;
	default:     break;
	}
	
	burn_complete_num = burn_complete_num | 0x02;
	if (burn_complete_num == 0x0f)
	{
		device1_flag = device4_flag = FALSE;

		INFO_STATE.EnableAllBtn();
		burn_complete_num = 0;
		burn_click_flag = 0;
		burn_stop_flag = FALSE;
	}
	return 2;
}

UINT Channe3_WorkForce(LPVOID pParam)
{
	CString path_app_bin;
	CString path_factory_bin;
	CString path_ota_bin;
	CString path_programmer_bin;
	switch (MODE) {
	case PRO_MODE:Burn_Progress(DEVICE3, path_app_bin, path_factory_bin, path_programmer_bin, path_ota_bin);
	break;
	case POW_MODE:Poweron(DEVICE3);
		break;
	case RTS_MODE:RTS_Process(DEVICE3);
	break;
	case FAC_MODE:FAS_Process(DEVICE3);
	break;
	default:     break;
	}
	burn_complete_num = burn_complete_num | 0x04;
	if (burn_complete_num == 0x0f)
	{
		device1_flag = device4_flag = FALSE;

		INFO_STATE.EnableAllBtn();
		burn_complete_num = 0;
		burn_click_flag = 0;
		burn_stop_flag = FALSE;
	}
	return 3;
}

UINT Channe4_WorkForce(LPVOID pParam)
{
	CString path_app_bin;
	CString path_factory_bin;
	CString path_ota_bin;
	CString path_programmer_bin;
	switch (MODE) {
	case PRO_MODE:Burn_Progress(DEVICE4, path_app_bin, path_factory_bin, path_programmer_bin, path_ota_bin);
	break;
	case POW_MODE:Poweron(DEVICE4);	
	break;
	case RTS_MODE:RTS_Process(DEVICE4); 
	break;
	case FAC_MODE:FAS_Process(DEVICE4); 
	break;
	default:     break;
	}
	burn_complete_num = burn_complete_num | 0x08;
	if (burn_complete_num == 0x0f)
	{
		device1_flag = device4_flag = FALSE;

		INFO_STATE.EnableAllBtn();
		burn_complete_num = 0;
		burn_click_flag = 0;
		burn_stop_flag = FALSE;
	}
	return 4;
}

UINT  Channe5_WorkRest(LPVOID lpParameter)
{

	

	
	return 5;

}

UINT Channe6_WorkForce(LPVOID lpParameter)
{
	Pass_COM_to_ScannerReading(SCAN_BLUE);
	return 6;
}


HBRUSH CproDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
	CBrush m_brush;
	CFont m_font;
	m_font.CreatePointFont(155,TEXT("宋体"));//代表15号字体，华文行楷
	m_brush.CreateSolidBrush(RGB(0, 255, 0));//画刷为绿色
	if (pWnd->GetDlgCtrlID() == IDC_STATIC1)
	{
		pDC->SetTextColor(RGB(0, 0, 250));
		pDC->SetBkColor(RGB(0, 255, 0));//背景色为绿色
		pDC->SetTextColor(RGB(255, 0, 0));//文字为红色
		pDC->SelectObject(&m_font);//文字为15号字体，华文行楷
		return m_brush;
	}
	if (pWnd->GetDlgCtrlID() == IDC_STATIC2)
	{
		pDC->SetTextColor(RGB(0, 0, 250));
		pDC->SetBkColor(RGB(0, 255, 0));//背景色为绿色
		pDC->SetTextColor(RGB(255, 0, 0));//文字为红色
		pDC->SelectObject(&m_font);//文字为15号字体，华文行楷
		return m_brush;
	}
	if (pWnd->GetDlgCtrlID() == IDC_STATIC3)
	{
		pDC->SetTextColor(RGB(0, 0, 250));
		pDC->SetBkColor(RGB(0, 255, 0));//背景色为绿色
		pDC->SetTextColor(RGB(255, 0, 0));//文字为红色
		pDC->SelectObject(&m_font);//文字为15号字体，华文行楷
		return m_brush;
	}
	if (pWnd->GetDlgCtrlID() == IDC_STATIC4)
	{
		pDC->SetTextColor(RGB(0, 0, 250));
		pDC->SetBkColor(RGB(0, 255, 0));//背景色为绿色
		pDC->SetTextColor(RGB(255, 0, 0));//文字为红色
		pDC->SelectObject(&m_font);//文字为15号字体，华文行楷
		return m_brush;
	}

	return hbr;
}



int change_count=0;

void CproDlg::OnEnChangeBtaddr0()
{

	change_count++;
	if (change_count > 0x08)
	{
		CString path;
		GetModuleFileName(NULL, path.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
		path.ReleaseBuffer();
		int pos = path.ReverseFind('\\');
		path = path.Left(pos);
		CString temp, strTip;
		
		CString strEditVidoe;
		int count = 0;
		int editnum = 0;
		int nStringLength = 0;
		for (count = IDC_BTADDR1; count < IDC_BLEADDR4 + 1; count++)
		{
			GetDlgItem(count)->GetWindowText(strEditVidoe);

			CString strTemp = strEditVidoe.SpanIncluding(TEXT("0123456789:ABCDEabcde"));
			if (strEditVidoe.GetLength() != 17 || strEditVidoe.GetLength() != strTemp.GetLength() || strEditVidoe.Find(TEXT(":"),0) != 2 || strEditVidoe.Find(TEXT(":"), 3) != 5 || strEditVidoe.Find(TEXT(":"), 6) != 8 || strEditVidoe.Find(TEXT(":"), 9) != 11 || strEditVidoe.Find(TEXT(":"), 12) != 14)
			{
				CWnd* pwnd = this->GetDlgItem(count);
				HWND hwnd = pwnd->GetSafeHwnd();
				strTip.Format(_T("取值范围应在%d~%d之间"), 1, 10);
				ShowBalloonTip(hwnd, strTip, _T("提示"), TTI_WARNING);
				//CBrush m_brush;
			//	m_brush.CreateSolidBrush(RGB(255, 0, 0));//画刷为绿色
				//GetDlgItem(count)->OnCtlColor();



			}


			// 不允许输入数字和点以外的字符


		}

		this->GetDlgItem(IDC_BLEADDR1)->GetWindowText(temp);
		WritePrivateProfileString(_T("addrinfo"), _T("Ble_addr1"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BLEADDR2)->GetWindowText(temp);
		WritePrivateProfileString(_T("addrinfo"), _T("Ble_addr2"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BLEADDR3)->GetWindowText(temp);
		WritePrivateProfileString(_T("addrinfo"), _T("Ble_addr3"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BLEADDR4)->GetWindowText(temp);
		WritePrivateProfileString(_T("addrinfo"), _T("Ble_addr4"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BTADDR1)->GetWindowText(temp);
		WritePrivateProfileString(_T("addrinfo"), _T("Bt_addr1"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BTADDR2)->GetWindowText(temp);
		WritePrivateProfileString(_T("addrinfo"), _T("Bt_addr2"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BTADDR3)->GetWindowText(temp);
		WritePrivateProfileString(_T("addrinfo"), _T("Bt_addr3"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BTADDR4)->GetWindowText(temp);
		WritePrivateProfileString(_T("addrinfo"), _T("Bt_addr4"), temp, path + TEXT("\\config.ini"));

		if (change_count == 100) change_count = 8;
	}

	
	
}

int change1_count = 0;
void CproDlg::OnEnChangename()
{
	//CString strEditVidoe;
	//int count = 0;
	//int editnum=0;
	//int nStringLength = 0;
	//for (count= IDC_BTADDR1;count < IDC_BLEADDR4+1;count++)
	//{
	//	GetDlgItem(count)->GetWindowText(strEditVidoe);
	//	nStringLength = strEditVidoe.GetLength();;
	//	



	//	// 不允许输入数字和点以外的字符
	//	for (int nIndexm = 0; nIndexm < nStringLength; nIndexm++)
	//	{
	//		if ((strEditVidoe[nIndexm] > 'f' || strEditVidoe[nIndexm] < '0'))
	//		{
	//			strEditVidoe = strEditVidoe.Left(nIndexm) + strEditVidoe.Right(strEditVidoe.GetLength() - nIndexm - 1);
	//				GetDlgItem(count)->SetWindowText(strEditVidoe);

	//		}
	//	}
	//	
	//}
	change1_count++;
	if (change1_count > 0x08)
	{
		CString path;
		GetModuleFileName(NULL, path.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
		path.ReleaseBuffer();
		int pos = path.ReverseFind('\\');
		path = path.Left(pos);
		CString temp;
		this->GetDlgItem(IDC_BLE1NAME)->GetWindowText(temp);
		WritePrivateProfileString(_T("nameinfo"), _T("Ble_name1"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BLE2NAME)->GetWindowText(temp);
		WritePrivateProfileString(_T("nameinfo"), _T("Ble_name2"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BLE3NAME)->GetWindowText(temp);
		WritePrivateProfileString(_T("nameinfo"), _T("Ble_name3"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BLE4NAME)->GetWindowText(temp);
		WritePrivateProfileString(_T("nameinfo"), _T("Ble_name4"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BT1NAME)->GetWindowText(temp);
		WritePrivateProfileString(_T("nameinfo"), _T("Bt_name1"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BT2NAME)->GetWindowText(temp);
		WritePrivateProfileString(_T("nameinfo"), _T("Bt_name2"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BT3NAME)->GetWindowText(temp);
		WritePrivateProfileString(_T("nameinfo"), _T("Bt_name3"), temp, path + TEXT("\\config.ini"));
		this->GetDlgItem(IDC_BT4NAME)->GetWindowText(temp);
		WritePrivateProfileString(_T("nameinfo"), _T("Bt_name4"), temp, path + TEXT("\\config.ini"));

		if (change1_count == 100) change1_count = 8;
	}



}



int Burn_Progress(CString device, CString path_app_bin, CString path_factory_bin, CString path_programmer_bin, CString path_ota_bin)
{
	int device_id = -1;
	if (device == DEVICE1) device_id = 0;
	if (device == DEVICE2) device_id = 1;
	if (device == DEVICE3) device_id = 2;
	if (device == DEVICE4) device_id = 3;

	int nport = -1;
	CString COM= TEXT("COM");
	CString btAddr, bleAddr,dangleaddr;
	CString	btname, blename;
	dangleaddr = TEXT("112233445566");

	nport = MTGetPortFromVidPid(device);
	if (nport == -1)
	{
		INFO_STATE.Device_state[device_id] = TEXT("状态：无设备");
	}
	else {
		INFO_STATE.Device_state[device_id] = TEXT("状态：发现设备");

		COM.Format(TEXT("COM%d"),nport);
		CString path;
		GetModuleFileName(NULL, path.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
		path.ReleaseBuffer();
		int pos = path.ReverseFind('\\');
		path = path.Left(pos);
		btAddr = INFO_STATE.Bt_Addr[device_id];
		bleAddr = INFO_STATE.Ble_Addr[device_id];
		btname = INFO_STATE.Bt_Name[device_id];
		blename = INFO_STATE.Ble_Name[device_id];
		bleAddr.Replace(TEXT(":"), TEXT(""));
		btAddr.Replace(TEXT(":"), TEXT(""));

		if (device == DEVICE1)
			path_factory_bin = path + TEXT("\\evocolabs0.bin");
		else if (device == DEVICE2)
			path_factory_bin = path + TEXT("\\evocolabs1.bin");
		else if (device == DEVICE3)
			path_factory_bin = path + TEXT("\\evocolabs2.bin");
		else if (device == DEVICE4) 
			path_factory_bin = path + TEXT("\\evocolabs3.bin");
		path_app_bin=INFO_STATE.Bin_path;
		path_ota_bin=INFO_STATE.Ota_path;

		
		path_programmer_bin = path + TEXT("\\programmer2300.bin");
		

		int iProcess = 0;
		CString cmdline_path = CString(COM) + TEXT(" \"") + CString(path_app_bin) + TEXT("\" \"") + CString(path_factory_bin) + TEXT("\" \"") + CString(path_programmer_bin) + TEXT("\" \"") + CString(path_ota_bin) + TEXT("\" ") + CString(btAddr) + TEXT(" ") + CString(bleAddr) + TEXT(" ") + CString(dangleAddr) + TEXT(" \"") + CString(btname) + TEXT("\" \"") + CString(blename) + TEXT("\"");

		STARTUPINFO startup_info;
		PROCESS_INFORMATION process_info;
		HANDLE hPipe = INVALID_HANDLE_VALUE;


		ZeroMemory(&process_info, sizeof(process_info));
		ZeroMemory(&startup_info, sizeof(startup_info));
		startup_info.cb = sizeof(startup_info);

		if (device_id == 0) 
		{
			cmdline_path = cmdline_path + TEXT(" DEVICE1");
			hPipe = CreateNamedPipe(
				TEXT("\\\\.\\Pipe\\Evocolabs1"),						//管道名
				PIPE_ACCESS_DUPLEX,									//管道类型 
				PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,	//管道参数
				PIPE_UNLIMITED_INSTANCES,							//管道能创建的最大实例数量
				0,													//输出缓冲区长度 0表示默认
				0,													//输入缓冲区长度 0表示默认
				NMPWAIT_WAIT_FOREVER,								//超时时间
				NULL);
			//指定一个SECURITY_ATTRIBUTES结构,或者传递零值.
		}
		else if (device_id == 1)
		{
			cmdline_path = cmdline_path + TEXT(" DEVICE2");
			hPipe = CreateNamedPipe(
				TEXT("\\\\.\\Pipe\\Evocolabs2"),						//管道名
				PIPE_ACCESS_DUPLEX,									//管道类型 
				PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,	//管道参数
				PIPE_UNLIMITED_INSTANCES,							//管道能创建的最大实例数量
				0,													//输出缓冲区长度 0表示默认
				0,													//输入缓冲区长度 0表示默认
				NMPWAIT_WAIT_FOREVER,								//超时时间
				NULL);
			//指定一个SECURITY_ATTRIBUTES结构,或者传递零值.
		}
		else if (device_id == 2)
		{
			cmdline_path = cmdline_path + TEXT(" DEVICE3");
			hPipe = CreateNamedPipe(
				TEXT("\\\\.\\Pipe\\Evocolabs3"),						//管道名
				PIPE_ACCESS_DUPLEX,									//管道类型 
				PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,	//管道参数
				PIPE_UNLIMITED_INSTANCES,							//管道能创建的最大实例数量
				0,													//输出缓冲区长度 0表示默认
				0,													//输入缓冲区长度 0表示默认
				NMPWAIT_WAIT_FOREVER,								//超时时间
				NULL);
			//指定一个SECURITY_ATTRIBUTES结构,或者传递零值.
		}
		else if (device_id == 3)
		{
			cmdline_path = cmdline_path + TEXT(" DEVICE4");
			hPipe = CreateNamedPipe(
				TEXT("\\\\.\\Pipe\\Evocolabs4"),						//管道名
				PIPE_ACCESS_DUPLEX,									//管道类型 
				PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,	//管道参数
				PIPE_UNLIMITED_INSTANCES,							//管道能创建的最大实例数量
				0,													//输出缓冲区长度 0表示默认
				0,													//输入缓冲区长度 0表示默认
				NMPWAIT_WAIT_FOREVER,								//超时时间
				NULL);
			//指定一个SECURITY_ATTRIBUTES结构,或者传递零值.
		}
		else
		{
			TRACE("设备不匹配");
			hPipe = INVALID_HANDLE_VALUE;

		}
		DWORD  x = 0;
		if (INFO_STATE.Is_erase[0])
			cmdline_path = cmdline_path + TEXT(" ") + CString("ERASE_TRUE");
		else
			cmdline_path = cmdline_path + TEXT(" ") + CString("ERASE_FALSE");

		wchar_t cmdline[1000];
		wsprintf(cmdline, cmdline_path.AllocSysString());

		char buf[256] = "";
		DWORD rlen = 0;
		int burn_state = 0;
		int timer_count = 0;
		int thread_bit = 0;
		UCHAR MASK_BIT = 0X00;
		CString windows_text, windows_text1, windows_text2, windows_text3;
		CString device1_state, device2_state, device3_state, device4_state;

		CString exe_path;
		GetModuleFileName(NULL, exe_path.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
		exe_path.ReleaseBuffer();
		int exe_pos = exe_path.ReverseFind('\\');
		exe_path = exe_path.Left(exe_pos);
		exe_path += TEXT("\\ConsoleApplication1.exe");


		if (INVALID_HANDLE_VALUE == hPipe)
		{
			TRACE("Create Pipe Error(%d)\n", GetLastError());
		}
		else
		{
			if (CreateProcess(exe_path, cmdline, 0, 0, 0, 0, 0, 0, &startup_info, &process_info) == 0)
			{
				TRACE("Error ");
			}
			printf("Waiting For Client Connection...\n");
			Sleep(10);

			if (ConnectNamedPipe(hPipe, NULL) == NULL)	//阻塞等待客户端连接。
			{
				printf("Connection failed!\n");
			}
			else
			{
				printf("Connection Success!\n");
			}

			while (!burn_state)
			{
				if (ReadFile(hPipe, buf, 256, &rlen, NULL) == FALSE) //接受客户端发送过来的内容
				{
					printf("Read Data From Pipe Failed!\n");
					break;
				}
				else
				{
					TRACE("From Client: data = %s, size = %d\n", buf, rlen);
					CString reinfor(buf);
					unsigned long notifyValue = 0;
					int pos = 0;
					pos = reinfor.Find(TEXT("ate:"));
					if (pos == 2) {

						reinfor = reinfor.Mid(pos + 4);
						notifyValue = _ttoi(reinfor);

						windows_text=INFO_STATE.Device_state[device_id];
						if (windows_text.Find(TEXT("状态：等待中")) == 0)
						{
							timer_count++;
							if (timer_count == 20)
							{
								timer_count = 0;
								break;
							}
							if (timer_count == 10)
							{
								if (thread_bit == 0)
								{
									thread_bit = 1;
									windows_text = INFO_STATE.Device_state[0];
									windows_text1 = INFO_STATE.Device_state[1];
									windows_text2 = INFO_STATE.Device_state[2];
									windows_text3 = INFO_STATE.Device_state[3];

									if (windows_text.Find(TEXT("状态：等待中")) == 0)  MASK_BIT = MASK_BIT | D0;
									if (windows_text1.Find(TEXT("状态：等待中")) == 0) MASK_BIT = MASK_BIT | D1;
									if (windows_text2.Find(TEXT("状态：等待中")) == 0) MASK_BIT = MASK_BIT | D2;
									if (windows_text3.Find(TEXT("状态：等待中")) == 0) MASK_BIT = MASK_BIT | D3;


									GPIO_SetDxConfig(SWITCH, MASK_BIT, 1);

									// enter CBUS bit-bang mode

									Sleep(500); //delay 50ms

									GPIO_SetDxConfig(SWITCH, MASK_BIT, 0);
									thread_bit = 0;
								}
								else {
									Sleep(1000); //delay 50ms
								}

							}
						}
						if (notifyValue == ev_wm_burn_progress) {
							pos = 0;
							pos = reinfor.Find(TEXT("rocess:"));
							if (pos != 0 && pos != -1)
							{
								reinfor = reinfor.Mid(pos + 7);
								iProcess = _ttoi(reinfor);
							}

						}
					}
					burn_state = MonitorDownloadEvent_CalibrateTrimAuto(device,notifyValue, iProcess);

					//添加暂停烧录功能
					if (burn_stop_flag == TRUE) {
						INFO_STATE.Device_state[device_id] = TEXT("状态：暂停烧写");

						TerminateProcess(process_info.hProcess, 0);
						CloseHandle(hPipe);//关闭管道

						return 0;
					}

					Sleep(100);

				}

			}
			INFO_STATE.Device_state[device_id]=TEXT("状态：关闭烧写");

			TerminateProcess(process_info.hProcess, 0);
			CloseHandle(hPipe);//关闭管道

		}


	}
	return 0;
}

UINT Poweron(CString device)
{




	return 0;

}

char* btaddr_conver(CString device, char btaddr[12])
{
	int devicenum = 0;
	

	return btaddr;

}

bool MonitorDownloadEvent_CalibrateTrimAuto(CString device, int notifyValue,  int iProcess)
{

		//TRACE("线程正在运行！\n");
	int device_id = -1;
	if (device == DEVICE1) device_id = 0;
	if (device == DEVICE2) device_id = 1;
	if (device == DEVICE3) device_id = 2;
	if (device == DEVICE4) device_id = 3;
		Sleep(10);
		switch (notifyValue)
		{
		case ev_wm_port_open_failed:
			INFO_STATE.Device_state[device_id]=TEXT("状态：串口打开失败");  return 1; break;
		case ev_wm_port_open_succeed:
			INFO_STATE.Device_state[device_id] = TEXT("状态：串口打开成功"); break;
		case ev_wm_sync_wait:
			INFO_STATE.Device_state[device_id] = TEXT("状态：等待中");
			
			break;
		case ev_wm_sync_failed:
			INFO_STATE.Device_state[device_id] = TEXT("状态：同步失败"); return 1; break;
		case ev_wm_sync_succeed:
			INFO_STATE.Device_state[device_id] = TEXT("状态：同步成功"); break;
		case ev_wm_run_programmer_failed:
			INFO_STATE.Device_state[device_id] = TEXT("状态：运行失败"); return 1; break;
		case ev_wm_run_programmer_succeed:
			INFO_STATE.Device_state[device_id] = TEXT("状态：运行成功"); break;
		case ev_wm_bin_encrypt_begin:
			INFO_STATE.Device_state[device_id] = TEXT("状态：加密开始"); break;
		case ev_wm_bin_encrypt_end:
			INFO_STATE.Device_state[device_id] = TEXT("状态：加密完成"); break;
		case ev_wm_update_sw_ver:
			INFO_STATE.Device_state[device_id] = TEXT("状态：更新软件版本"); break;
		case ev_wm_update_product_id:
			INFO_STATE.Device_state[device_id] = TEXT("状态：更新制造数字"); break;
		case ev_wm_burn_progress:
			INFO_STATE.Device_state[device_id] = TEXT("状态：烧写中");
			
			
				INFO_STATE.Device_state[device_id] = TEXT("状态：烧写中");
				INFO_STATE.Burn_Progress[device_id] = iProcess;

				

				
			
			break;
		case ev_wm_burn_magic:
			INFO_STATE.Device_state[device_id] = TEXT("状态：烧写magic"); break;
		case ev_wm_burn_failure:
			INFO_STATE.Device_state[device_id] = TEXT("状态：烧写失败");
			return 1; break;
		case ev_wm_burn_complt:
			INFO_STATE.Device_state[device_id] = TEXT("状态：烧写完成");
			break;
		case ev_wm_burn_efuse_start:
			INFO_STATE.Device_state[device_id] = TEXT("状态：烧写efuse开始"); break;
		case ev_wm_burn_efuse_end:
			INFO_STATE.Device_state[device_id] = TEXT("状态：烧写efuse结束"); break;
		case ev_wm_factory_mode:
			INFO_STATE.Device_state[device_id] = TEXT("状态：工厂模式"); break;
		case ev_wm_block_for_audition:
			INFO_STATE.Device_state[device_id] = TEXT("状态：aud阻塞"); break;
		case ev_wm_audition_failure:
			INFO_STATE.Device_state[device_id] = TEXT("状态：aud失败"); break;
		case ev_wm_burn_audsec_success:
			INFO_STATE.Device_state[device_id] = TEXT("状态：烧写aud成功"); break;
		case ev_wm_burn_audsec_failure:
			INFO_STATE.Device_state[device_id] = TEXT("状态：烧写aud失败"); break;
		case ev_wm_chip_poweroff:
			INFO_STATE.Device_state[device_id] = TEXT("状态：芯片关机"); break;
		case ev_wm_ready_next_work:
			INFO_STATE.Device_state[device_id] = TEXT("状态：准备下一个工作"); break;
		case ev_wm_exit_valid:
			INFO_STATE.Device_state[device_id] = TEXT("状态：退出成功");
			return 1;
			break;
		case ev_wm_exit_invalid:
			INFO_STATE.Device_state[device_id] = TEXT("状态：退出失败");
			return 1;
			break;
		case ev_wm_factory_mode_success:
			INFO_STATE.Device_state[device_id] = TEXT("状态：工厂模式成功");
			return 1;
			break;
		case ev_wm_factory_mode_progress:

			INFO_STATE.Device_state[device_id] = TEXT("状态：工厂模式进程"); break;
		case ev_wm_factory_mode_fail:
			INFO_STATE.Device_state[device_id] = TEXT("状态：工厂模式失败");  break;
		case ev_wm_factory_calib_value:
			INFO_STATE.Device_state[device_id] = TEXT("状态：工厂校准"); break;
		case ev_wm_exit_user_stop:
			INFO_STATE.Device_state[device_id] = TEXT("状态：用户停止");  break;
		case ev_wm_read_success:
			INFO_STATE.Device_state[device_id] = TEXT("状态：读取成功"); break;
		case ev_wm_read_fail:
			INFO_STATE.Device_state[device_id] = TEXT("状态：读取失败");  break;
			return 1;
			break;
		case BURN_PREPAR:
			INFO_STATE.Device_state[device_id] = TEXT("状态：准备烧写");  break;

			break;
			
		case ev_wm_max:return 0;
			break;
		}
		return 0;
	
}
DWORD thread_bit = 0;
void BurnCalibrateTrimAuto(CString device, CString COM,CString btAddr, CString bleAddr, CString dangleAddr, CString bt_name, CString ble_name, CString path_app_bin, CString path_factory_bin, CString path_programmer_bin, CString path_ota_bin)
{

}


/************************************************************************/
/* 根据USB描述信息字符串中读取
/************************************************************************/
int MTGetPortFromVidPid(CString strVidPid)
{
	// 获取当前系统所有使用的设备
	int					nPort = -1;
	int					nStart = -1;
	int					nEnd = -1;
	int					i = 0;
	CString				strTemp, strName;
	DWORD				dwFlag = (DIGCF_ALLCLASSES | DIGCF_PRESENT);
	HDEVINFO			hDevInfo = INVALID_HANDLE_VALUE;
	SP_DEVINFO_DATA		sDevInfoData;
	TCHAR				szDis[MAX_PATH] = { 0x00 };// 存储设备实例ID
	TCHAR				szFN[MAX_PATH] = { 0x00 };// 存储设备实例属性
	DWORD				nSize = 0;

	// 准备遍历所有设备查找USB
	hDevInfo = SetupDiGetClassDevs(NULL, NULL, NULL, dwFlag);
	if (INVALID_HANDLE_VALUE == hDevInfo)
		goto STEP_END;

	// 开始遍历所有设备
	memset(&sDevInfoData, 0x00, sizeof(SP_DEVICE_INTERFACE_DATA));
	sDevInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
	for (i = 0; SetupDiEnumDeviceInfo(hDevInfo, i, &sDevInfoData); i++)
	{
		nSize = 0;

		// 无效设备
		if (!SetupDiGetDeviceInstanceId(hDevInfo, &sDevInfoData, szDis, sizeof(szDis), &nSize))
			goto STEP_END;

		// 根据设备信息寻找VID PID一致的设备
		strTemp.Format(_T("%s"), szDis);
		strTemp.MakeUpper();
		if (strTemp.Find(strVidPid, 0) == -1)
			continue;
		// 查找设备属性
		nSize = 0;
		SetupDiGetDeviceRegistryProperty(hDevInfo, &sDevInfoData,
			SPDRP_FRIENDLYNAME,
			0, (PBYTE)szFN,
			sizeof(szFN),
			&nSize);

		// "XXX Virtual Com Port (COM7)"
		strName.Format(_T("%s"), szFN);
		if (strName.IsEmpty())
			goto STEP_END;

		// 寻找串口信息
		nStart = strName.Find(_T("(COM"), 0);
		nEnd = strName.Find(_T(")"), 0);
		if (nStart == -1 || nEnd == -1)
			goto STEP_END;

		strTemp = strName.Mid(nStart + 4, nEnd - nStart - 2);
		nPort = _tstoi(LPCTSTR(strTemp));
	}

STEP_END:
	// 关闭设备信息集句柄
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		SetupDiDestroyDeviceInfoList(hDevInfo);
		hDevInfo = INVALID_HANDLE_VALUE;
	}

	return nPort;
}


void CproDlg::OnBnClickedChoosebin()
{
	CString gReadFilePathName;
	CString path;
	GetModuleFileName(NULL, path.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
	path.ReleaseBuffer();
	int pos = path.ReverseFind('\\');
	path = path.Left(pos);
	path += TEXT("\\config.ini");
	CFileDialog fileDlg(true, _T("bin"), _T("*.bin"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("bin Files (*.bin)|*.bin|All File (*.*)|*.*||"), NULL);
	if (fileDlg.DoModal() == IDOK)    //弹出对话框  
	{
		gReadFilePathName = fileDlg.GetPathName();//得到完整的文件名和目录名拓展名  
		GetDlgItem(IDC_PATHBIN)->SetWindowText(gReadFilePathName);//将路径显示  
		CString filename = fileDlg.GetFileName();
		WritePrivateProfileString(_T("pathinfo"), _T("PATHBIN"), gReadFilePathName, path);

	}
}


void CproDlg::OnBnClickedChooseota()
{
	CString gReadFilePathName;
	CString path;
	GetModuleFileName(NULL, path.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
	path.ReleaseBuffer();
	int pos = path.ReverseFind('\\');
	path = path.Left(pos);
	path += TEXT("\\config.ini");
	CFileDialog fileDlg(true, _T("bin"), _T("*.bin"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("bin Files (*.bin)|*.bin|All File (*.*)|*.*||"), NULL);
	if (fileDlg.DoModal() == IDOK)    //弹出对话框  
	{
		gReadFilePathName = fileDlg.GetPathName();//得到完整的文件名和目录名拓展名  
		GetDlgItem(IDC_PATHOTA)->SetWindowText(gReadFilePathName);//将路径显示  
		CString filename = fileDlg.GetFileName();
		WritePrivateProfileString(_T("pathinfo"), _T("PATHOTA"), gReadFilePathName, path);
	}
}


void CproDlg::OnBnClickedRts()
{
	if (Developer_Mode == 0) {
		LPVOID                 pParam = NULL;
		int                    nPriority = THREAD_PRIORITY_HIGHEST;//默认为THREAD_PRIORITY_NORMAL
		UINT                   nStackSize = 0;//与创建它的线程堆栈大小相同
		DWORD                  dwCreateFlags = 0;//创建后立即执行
		LPSECURITY_ATTRIBUTES  lpSecurityAttrs = NULL;//与创建它的线程安全属性相同
		

		INFO_STATE.DisableAllBtn();
		MODE = RTS_MODE;
		Channel_Thread = AfxBeginThread((AFX_THREADPROC)Channel_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
		Channe2_Thread = AfxBeginThread((AFX_THREADPROC)Channe2_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
		Channe3_Thread = AfxBeginThread((AFX_THREADPROC)Channe3_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
		Channe4_Thread = AfxBeginThread((AFX_THREADPROC)Channe4_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
	}

	if (Developer_Mode == 1) {
		INFO_STATE.Device_state[0] = TEXT("状态：开始复位");
		INFO_STATE.Device_state[1] = TEXT("状态：开始复位");
		INFO_STATE.Device_state[2] = TEXT("状态：开始复位");
		INFO_STATE.Device_state[3] = TEXT("状态：开始复位");
		INFO_STATE.DisableAllBtn();

		bool ftstate = true;
		ftstate &= GPIO_SetDxConfig(SWITCH, D0 | D1 | D2 | D3, 1);
		if (ftstate == false)
		{
			INFO_STATE.Device_state[0] = TEXT("状态：复位失败");
			INFO_STATE.Device_state[1] = TEXT("状态：复位失败");
			INFO_STATE.Device_state[2] = TEXT("状态：复位失败");
			INFO_STATE.Device_state[3] = TEXT("状态：复位失败");
		}
		Sleep(200);
		ftstate &= GPIO_SetDxConfig(SWITCH, D0 | D1 | D2 | D3, 0);
		if (ftstate == false)
		{
			INFO_STATE.Device_state[0] = TEXT("状态：复位失败");
			INFO_STATE.Device_state[1] = TEXT("状态：复位失败");
			INFO_STATE.Device_state[2] = TEXT("状态：复位失败");
			INFO_STATE.Device_state[3] = TEXT("状态：复位失败");
		}
		if (ftstate == true)
		{
			INFO_STATE.Device_state[0] = TEXT("状态：复位成功");
			INFO_STATE.Device_state[1] = TEXT("状态：复位成功");
			INFO_STATE.Device_state[2] = TEXT("状态：复位成功");
			INFO_STATE.Device_state[3] = TEXT("状态：复位成功");
		}

		Reset_BTBLEAddr(NULL);
		INFO_STATE.EnableAllBtn();

	}

		
	
}


UINT RTS_Process(CString device)
{
	int device_id = -1;
	if (device == DEVICE1) device_id = 0;
	if (device == DEVICE2) device_id = 1;
	if (device == DEVICE3) device_id = 2;
	if (device == DEVICE4) device_id = 3;

	int nport = -1;
	CString COM = TEXT("COM"); 
	nport = MTGetPortFromVidPid(device);

	if (nport == -1)
	{
		INFO_STATE.Device_state[device_id]=TEXT("状态：无设备");
	}
	else {
		COM.Format(TEXT("\\\\.\\COM%d"), nport);
		HANDLE	hCom;
		hCom = CreateFile(COM, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);  
		if (hCom == (HANDLE)-1)
		{
			INFO_STATE.Device_state[device_id] = TEXT("状态：打开失败");

			CloseHandle(hCom);
			return false;
		}
		else
		{
			COMMTIMEOUTS TimeOuts;
			//设定读超时
			TimeOuts.ReadIntervalTimeout = MAXDWORD;
			TimeOuts.ReadTotalTimeoutMultiplier = 0;
			TimeOuts.ReadTotalTimeoutConstant = 0;
			//在读一次输入缓冲区的内容后读操作就立即返回，
			//而不管是否读入了要求的字符。


			//设定写超时
			TimeOuts.WriteTotalTimeoutMultiplier = 100;
			TimeOuts.WriteTotalTimeoutConstant = 500;
			SetCommTimeouts(hCom, &TimeOuts); //设置超时
			SetupComm(hCom, 1024, 1024); //输入缓冲区和输出缓冲区的大小都是1024
			DCB wdcb;
			GetCommState(hCom, &wdcb);
			wdcb.BaudRate = 921600;//波特率：921600，其他：不变
			wdcb.ByteSize = 8;   //每个字节8位
			wdcb.StopBits = ONESTOPBIT;
			wdcb.Parity = NOPARITY;  //无停止位
			SetCommState(hCom, &wdcb);
			PurgeComm(hCom, PURGE_TXCLEAR | PURGE_RXCLEAR);
		}

		char* sendbuff = "[RELEASEMODE,]"; 
		DWORD wCount;
		DWORD rCount, Time = 0, count = 0;
		//TCHAR* recbuff = TEXT("");
		static COMSTAT ComStat;
		//DWORD dwBytesRead = 0;
		//dwBytesRead = min(dwBytesRead, (DWORD)ComStat.cbInQue);
		char str[1000];
		BOOL bReadStat;
		BOOL bWriteStat;
		CString re;
		clock_t start = 0, finish = 0;
		bWriteStat = WriteFile(hCom, sendbuff, 14, &wCount, NULL);//发送数据
		if (!bWriteStat) {
			INFO_STATE.Device_state[device_id] = TEXT("状态：串口读取失败");
		}
		else {
			start = clock();
			while (true)
			{
				finish = clock();
				if ((finish - start) > 1000) {
					INFO_STATE.Device_state[device_id] = TEXT("状态：进入参考模式失败");
					break;
				}
				bReadStat = ReadFile(hCom, str, 1000, &rCount, NULL);
				if (!bReadStat) {
					printf("读串口失败!");
					INFO_STATE.Device_state[device_id] = TEXT("状态：串口读取失败");
					break;
				}
				else
				{
					if (rCount >= 1000)
						rCount = 999;
					str[rCount] = '\0';
					printf("%s", str);
					if ((CString(str).Find(TEXT("Shut down system")) != -1) | (CString(str).Find(TEXT("factory release")) != -1)) {
						INFO_STATE.Device_state[device_id] = TEXT("状态：进入参考模式成功");
						break;
					}
				}
			}
		}
		CloseHandle(hCom);
	    free(sendbuff);
	}
	return 0;


}


int fac_click_flag=0;
void CproDlg::OnBnClickedFac()
{
	if (Developer_Mode == 0) {
		LPVOID                 pParam = NULL;
		int                    nPriority = THREAD_PRIORITY_HIGHEST;//默认为THREAD_PRIORITY_NORMAL
		UINT                   nStackSize = 0;//与创建它的线程堆栈大小相同
		DWORD                  dwCreateFlags = 0;//创建后立即执行
		LPSECURITY_ATTRIBUTES  lpSecurityAttrs = NULL;//与创建它的线程安全属性相同


		INFO_STATE.DisableAllBtn();
		MODE = FAC_MODE;
		Channel_Thread = AfxBeginThread((AFX_THREADPROC)Channel_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
		Channe2_Thread = AfxBeginThread((AFX_THREADPROC)Channe2_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
		Channe3_Thread = AfxBeginThread((AFX_THREADPROC)Channe3_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
		Channe4_Thread = AfxBeginThread((AFX_THREADPROC)Channe4_WorkForce, pParam, nPriority, nStackSize, dwCreateFlags, lpSecurityAttrs);
	}
	
	if (Developer_Mode == 1) {
		BOOL ftstate = true;
		if (fac_click_flag % 2 == 0)
		{
			INFO_STATE.Device_state[0] = TEXT("状态：开始充电");
			INFO_STATE.Device_state[1] =  TEXT("状态：开始充电");
			INFO_STATE.Device_state[2] = TEXT("状态：开始充电");
			INFO_STATE.Device_state[3] = TEXT("状态：开始充电");
			this->SetDlgItemText(IDC_FAC, TEXT("已充电"));

			ftstate &= GPIO_SetDxConfig(SWITCH, D4|D5|D6|D7, 1);



			if (ftstate == false)
			{
				INFO_STATE.Device_state[0] = TEXT("状态：充电失败");
				INFO_STATE.Device_state[1] = TEXT("状态：充电失败");
				INFO_STATE.Device_state[2] = TEXT("状态：充电失败");
				INFO_STATE.Device_state[3] = TEXT("状态：充电失败");
				this->SetDlgItemText(IDC_FAC, TEXT("未充电"));
			}
		}

		if (fac_click_flag % 2 == 1)
		{
			ftstate &= GPIO_SetDxConfig(SWITCH, D4 | D5 | D6 | D7, 0);
			if (ftstate == true)
			{
				INFO_STATE.Device_state[0] = TEXT("状态：充电完成");
				INFO_STATE.Device_state[1] = TEXT("状态：充电完成");
				INFO_STATE.Device_state[2] = TEXT("状态：充电完成");
				INFO_STATE.Device_state[3] = TEXT("状态：充电完成");
				this->SetDlgItemText(IDC_FAC, TEXT("未充电"));
			}
		}
		fac_click_flag++;
		if (fac_click_flag == 10) fac_click_flag = 0;
	}

}

UINT FAS_Process(CString device)
{
	//To do: 接出厂模式按钮的飞线？

	
	return 0;
}






void CproDlg::OnAcceleratorCtrlD()
{
	if (this->GetDlgItem(IDC_BTLABE)->IsWindowVisible())
	{
		Developer_Mode = 0;
		for (int i = IDC_BTLABE; i < IDC_LOG4 + 1; i++)
			this->GetDlgItem(i)->ShowWindow(false);
		this->GetDlgItem(IDC_PRO)->SetWindowText(TEXT("烧录"));
		this->GetDlgItem(IDC_POW)->SetWindowText(TEXT("开机"));
		this->GetDlgItem(IDC_RTS)->SetWindowText(TEXT("参考模式"));
		this->GetDlgItem(IDC_FAC)->SetWindowText(TEXT("出厂设置"));
		this->GetDlgItem(IDC_PRO)->SetWindowPos(NULL, PRO_XPOSITION, BUTTON_YPOSITION, BUTTON_LENGTH, BUTTON_WIDTH, SWP_NOZORDER);
		this->GetDlgItem(IDC_POW)->SetWindowPos(NULL, POW_XPOSITION, BUTTON_YPOSITION, BUTTON_LENGTH, BUTTON_WIDTH, SWP_NOZORDER);
		this->GetDlgItem(IDC_RTS)->SetWindowPos(NULL, RTS_XPOSITION, BUTTON_YPOSITION, BUTTON_LENGTH, BUTTON_WIDTH, SWP_NOZORDER);
		this->GetDlgItem(IDC_FAC)->SetWindowPos(NULL, FAC_XPOSITION, BUTTON_YPOSITION, BUTTON_LENGTH, BUTTON_WIDTH, SWP_NOZORDER);

		CFont* font = new	CFont;
		CFont* font1 = new	CFont;
		font->CreateFont(100, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, TEXT("华文楷体"));
		this->GetDlgItem(IDC_PRO)->SetFont(font);
		this->GetDlgItem(IDC_POW)->SetFont(font);
		font1->CreateFont(50, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, TEXT("华文楷体"));
		this->GetDlgItem(IDC_RTS)->SetFont(font1);
		this->GetDlgItem(IDC_FAC)->SetFont(font1);
	}
	else
	{
		Developer_Mode = 1;
		for (int i = IDC_BTLABE; i < IDC_LOG4 + 1; i++)
			this->GetDlgItem(i)->ShowWindow(true);

		this->GetDlgItem(IDC_PRO)->SetWindowText(TEXT("BURN"));
		this->GetDlgItem(IDC_POW)->SetWindowText(TEXT("POWERON"));
		this->GetDlgItem(IDC_RTS)->SetWindowText(TEXT("RESET"));
		this->GetDlgItem(IDC_FAC)->SetWindowText(TEXT("CHARGER"));
		this->GetDlgItem(IDC_PRO)->SetWindowPos(NULL, PRO_XPOSITION, BUTTON_YPOSITION, BUTTON_LENGTH, BUTTON_WIDTH, SWP_NOZORDER);
		this->GetDlgItem(IDC_POW)->SetWindowPos(NULL, RTS_XPOSITION, BUTTON_YPOSITION, BUTTON_LENGTH, BUTTON_WIDTH, SWP_NOZORDER);
		this->GetDlgItem(IDC_RTS)->SetWindowPos(NULL, POW_XPOSITION, BUTTON_YPOSITION, BUTTON_LENGTH, BUTTON_WIDTH, SWP_NOZORDER);
		this->GetDlgItem(IDC_FAC)->SetWindowPos(NULL, FAC_XPOSITION, BUTTON_YPOSITION, BUTTON_LENGTH, BUTTON_WIDTH, SWP_NOZORDER);
		CFont* font = new	CFont;
		CFont* font1 = new	CFont;
		font->CreateFont(20, 0, 0, 0, 0, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, TEXT("Arial"));
		this->GetDlgItem(IDC_PRO)->SetFont(font);
		this->GetDlgItem(IDC_POW)->SetFont(font);
		font1->CreateFont(20, 0, 0, 0, 0, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, TEXT("Arial"));
		this->GetDlgItem(IDC_RTS)->SetFont(font1);
		this->GetDlgItem(IDC_FAC)->SetFont(font1);
	}

}

BOOL CproDlg::PreTranslateMessage(MSG* pMsg)
{

	if (m_hAccel)
	{
		if (::TranslateAccelerator(m_hWnd, m_hAccel, pMsg))
		{
			return(TRUE);
		}
		else
		{
			if(pMsg->message == WM_KEYDOWN)
			{
				
				 if(GetAsyncKeyState(VK_CONTROL))
				{
					if (pMsg->wParam == 'D')
						//加判断是否内网
					
						OnAcceleratorCtrlD();
					
				}
			}
		}
	}
	return   CDialog::PreTranslateMessage(pMsg);

}


void CproDlg::ReSize(void)
{
	float fsp[2];
	POINT Newp; //获取现在对话框的大小
	CRect recta;
	GetClientRect(&recta);     //取客户区大小  
	Newp.x = recta.right - recta.left;
	Newp.y = recta.bottom - recta.top;
	fsp[0] = (float)Newp.x / old.x;
	fsp[1] = (float)Newp.y / old.y;
	CRect Rect;
	int woc;
	CPoint OldTLPoint, TLPoint; //左上角
	CPoint OldBRPoint, BRPoint; //右下角
	HWND  hwndChild = ::GetWindow(m_hWnd, GW_CHILD);  //列出所有控件  
	while (hwndChild)
	{
		woc = ::GetDlgCtrlID(hwndChild);//取得ID
		GetDlgItem(woc)->GetWindowRect(Rect);
		ScreenToClient(Rect);
		OldTLPoint = Rect.TopLeft();
		TLPoint.x = long(OldTLPoint.x * fsp[0]);
		TLPoint.y = long(OldTLPoint.y * fsp[1]);
		OldBRPoint = Rect.BottomRight();
		BRPoint.x = long(OldBRPoint.x * fsp[0]);
		BRPoint.y = long(OldBRPoint.y * fsp[1]);
		Rect.SetRect(TLPoint, BRPoint);
		GetDlgItem(woc)->MoveWindow(Rect, TRUE);
		hwndChild = ::GetWindow(hwndChild, GW_HWNDNEXT);
	}
	old = Newp;


}

void CproDlg::OnBnClickedUpdate()
{

	if (this->GetDlgItem(IDC_BT4NAME)->IsWindowEnabled())
	{
		for (int i = IDC_BTADDR1; i < IDC_BT4NAME + 1; i++)
			if (i == IDC_PATHOTA || i == IDC_BTLABE16 || i == IDC_BTLABE17);
			else this->GetDlgItem(i)->EnableWindow(false);
	}
	else
	{
		for (int i = IDC_BTADDR1; i < IDC_BT4NAME + 1; i++)
			if (i == IDC_PATHOTA || i == IDC_BTLABE16 || i == IDC_BTLABE17);
			else this->GetDlgItem(i)->EnableWindow(true);
	}
	
}


void CproDlg::OnBnClickedErase()
{

		CString path; 
		GetModuleFileName(NULL, path.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
		path.ReleaseBuffer();
		int pos = path.ReverseFind('\\');
		path = path.Left(pos);
		CString temp;
		if (INFO_STATE.Is_erase[0])
			WritePrivateProfileString(_T("checkinfo"), _T("erase"), TEXT("ERASE_TRUE"), path + TEXT("\\config.ini"));
		else
			WritePrivateProfileString(_T("checkinfo"), _T("erase"), TEXT("ERASE_FALSE"), path + TEXT("\\config.ini"));

}


void CproDlg::ShowBalloonTip(HWND hWnd, LPCTSTR lpszText, LPCTSTR lpszTittle, int nTTIcon)
{
#ifdef _UNICODE
	EDITBALLOONTIP tagTip;
	tagTip.cbStruct = sizeof(EDITBALLOONTIP);
	tagTip.pszText = lpszText;
	tagTip.pszTitle = lpszTittle;
	tagTip.ttiIcon = nTTIcon;
	Edit_ShowBalloonTip(hWnd, &tagTip);//加打错字直接红起来，button灰掉
#else
	MessageBox(lpszText, lpszTittle);
#endif

	//((CEdit*)CWnd::FromHandle(hWnd))->SetSel(0, -1);
}



void CproDlg::OnTimer(UINT_PTR nIDEvent)
{
	Pass_ScannerReading_to_BTBLEAddr(INFO_STATE.Scanner_state[0]);

	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (nIDEvent == 0)
	{
		for (int i = 0; i < 4; i++)
		{
			this->GetDlgItem(IDC_STATE1 + i)->SetWindowText(INFO_STATE.Device_state[i]);
			this->GetDlgItem(IDC_BTADDR1 + i)->GetWindowText(INFO_STATE.Bt_Addr[i]);
			this->GetDlgItem(IDC_BLEADDR1 + i)->GetWindowText(INFO_STATE.Ble_Addr[i]);
			this->GetDlgItem(IDC_BLE1NAME + i)->GetWindowText(INFO_STATE.Ble_Name[i]);
			this->GetDlgItem(IDC_BT1NAME + i)->GetWindowText(INFO_STATE.Bt_Name[i]);
			
		}
		prog.SetPos(INFO_STATE.Burn_Progress[0]);
		prog4.SetPos(INFO_STATE.Burn_Progress[3]);
		prog2.SetPos(INFO_STATE.Burn_Progress[1]);
		prog3.SetPos(INFO_STATE.Burn_Progress[2]);
		this->GetDlgItem(IDC_PATHBIN)->GetWindowText(INFO_STATE.Bin_path);
		this->GetDlgItem(IDC_PATHOTA)->GetWindowText(INFO_STATE.Ota_path);
		INFO_STATE.Is_erase[0] = this->IsDlgButtonChecked(IDC_ERASE);
		this->GetDlgItem(IDC_PRO)->EnableWindow(INFO_STATE.Is_Burn_Enable);
		this->GetDlgItem(IDC_POW)->EnableWindow(INFO_STATE.Is_Pow_Enable);
		this->GetDlgItem(IDC_RTS)->EnableWindow(INFO_STATE.Is_RTS_Enable);
		this->GetDlgItem(IDC_FAC)->EnableWindow(INFO_STATE.Is_Fac_Enable);
		this->GetDlgItem(IDC_LOG1)->EnableWindow(INFO_STATE.Is_Log1_Enable);
		this->GetDlgItem(IDC_LOG2)->EnableWindow(INFO_STATE.Is_Log2_Enable);
		this->GetDlgItem(IDC_LOG3)->EnableWindow(INFO_STATE.Is_Log3_Enable);
		this->GetDlgItem(IDC_LOG4)->EnableWindow(INFO_STATE.Is_Log4_Enable);
	}

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CproDlg::OnDeviceChange(UINT nEventType, DWORD dwData)
{
	int nport = -1;
	//CString COM = TEXT("COM");
	//DEV_BROADCAST_DEVICEINTERFACE* dbd = (DEV_BROADCAST_DEVICEINTERFACE*) dwData; 
	//这两行没看懂

	switch (nEventType)
	{
	
	case DBT_DEVICEARRIVAL://添加设备
		nport = MTGetPortFromVidPid(SCAN_BLUE);
		if (nport != -1)
		{
			this->GetDlgItem(IDC_FINDDEVICE)->SetWindowText(TEXT("扫码枪插入"));//需要检测拔出
			nport = -1;
		}
		break;

	case DBT_DEVICEREMOVECOMPLETE://移除设备
		this->GetDlgItem(IDC_FINDDEVICE)->SetWindowText(TEXT("扫码枪拔出"));
		
		break;

	default:
		break;
	}
	return TRUE;
}


void CproDlg::OnBnClickedLog1()
{
	CDevicelog* Devicelog1;
	Devicelog1 = new CDevicelog(NULL,1);
	Devicelog1->Create(IDD_DIALOG_LOG, this);
	Devicelog1->ShowWindow(SW_SHOW);
}

void CproDlg::OnBnClickedLog2()
{
	// TODO: 在此添加控件通知处理程序代码
	CDevicelog* Devicelog2;
	Devicelog2 = new CDevicelog(NULL, 2);
	Devicelog2->Create(IDD_DIALOG_LOG, this);
	Devicelog2->ShowWindow(SW_SHOW);
}


void CproDlg::OnBnClickedLog3()
{
	// TODO: 在此添加控件通知处理程序代码
	CDevicelog* Devicelog3;
	Devicelog3 = new CDevicelog(NULL, 3);
	Devicelog3->Create(IDD_DIALOG_LOG, this);
	Devicelog3->ShowWindow(SW_SHOW);
}


void CproDlg::OnBnClickedLog4()
{
	// TODO: 在此添加控件通知处理程序代码
	CDevicelog* Devicelog4;
	Devicelog4 = new CDevicelog(NULL, 4);
	Devicelog4->Create(IDD_DIALOG_LOG, this);
	Devicelog4->ShowWindow(SW_SHOW);
}