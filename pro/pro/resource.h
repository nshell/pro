//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 pro.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PRO_DIALOG                  102
#define IDR_ACCELERATOR1                131
#define IDI_ICON1                       134
#define IDB_PNG1                        137
#define IDB_BITMAP1                     138
#define IDD_DIALOG_LOG                  143
#define IDC_PRO                         1000
#define IDC_POW                         1001
#define IDC_RTS                         1002
#define IDC_FAC                         1003
#define IDC_PROGRESS1                   1004
#define IDC_PROGRESS2                   1005
#define IDC_PROGRESS3                   1006
#define IDC_PROGRESS4                   1007
#define IDC_STATIC1                     1008
#define IDC_STATIC2                     1009
#define IDC_STATIC3                     1010
#define IDC_STATIC4                     1011
#define IDC_STATE1                      1012
#define IDC_STATE2                      1013
#define IDC_STATE3                      1014
#define IDC_STATE4                      1015
#define IDC_BTLABE                      1016
#define IDC_BTLABE2                     1017
#define IDC_BTLABE3                     1018
#define IDC_BTLABE4                     1019
#define IDC_BTLABE5                     1020
#define IDC_BTLABE6                     1021
#define IDC_BTLABE7                     1022
#define IDC_BTLABE8                     1023
#define IDC_BTLABE9                     1024
#define IDC_BTLABE14                    1025
#define IDC_BTLABE15                    1026
#define IDC_CHOOSEBIN                   1027
#define IDC_PATHBIN                     1028
#define IDC_CHOOSEOTA                   1029
#define IDC_BTADDR1                     1030
#define IDC_BTADDR2                     1031
#define IDC_BTADDR3                     1032
#define IDC_BTADDR4                     1033
#define IDC_BLEADDR1                    1034
#define IDC_BLEADDR2                    1035
#define IDC_BLEADDR3                    1036
#define IDC_BLEADDR4                    1037
#define IDC_PATHOTA                     1038
#define IDC_EDIT2                       1039
#define IDC_BLE1NAME                    1039
#define IDC_BLE2NAME                    1040
#define IDC_BLE3NAME                    1041
#define IDC_BLE4NAME                    1042
#define IDC_BTLABE17                    1043
#define IDC_BTLABE16                    1044
#define IDC_BT1NAME                     1045
#define IDC_BT2NAME                     1046
#define IDC_BT3NAME                     1047
#define IDC_BT4NAME                     1048
#define IDC_BTLABE18                    1049
#define IDC_BTLABE19                    1050
#define IDC_BTLABE20                    1051
#define IDC_BTLABE21                    1052
#define IDC_BTLABE10                    1053
#define IDC_BTLABE11                    1054
#define IDC_BTLABE12                    1055
#define IDC_BTLABE13                    1056
#define IDC_OTALABE                     1056
#define IDC_BINLABE                     1057
#define IDC_ERASE                       1058
#define IDC_LOG1                        1059
#define IDC_LOG2                        1060
#define IDC_LOG3                        1061
#define IDC_LOG4                        1062
#define IDC_PICTURE                     1063
#define IDC_FINDDEVICE                  1064
#define IDCANCEL                        1065
#define IDC_BUTTON123                   1066
#define IDC_LOG_DATA                    1067
#define IDC_LOG_STATE                   1068
#define IDC_LOG_ID                      1069
#define IDC_COMBO1                      1071
#define IDC_SCANNER                     1072
#define IDC_BTNSEND                     1073
#define IDC_TEXT2SEND                   1074
#define IDC_READ                        1075
#define ID_ACCELERATOR_CTRL_D           32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        145
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1076
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
