#include "stdafx.h"
#include "display_info.h"

void display_info::EnableAllBtn(){
	this->Is_RTS_Enable = TRUE;
	this->Is_Pow_Enable = TRUE;
	this->Is_Burn_Enable = TRUE;
	this->Is_Fac_Enable = TRUE;
	this->Is_Log1_Enable = TRUE;
	this->Is_Log2_Enable = TRUE;
	this->Is_Log3_Enable = TRUE;
	this->Is_Log4_Enable = TRUE;
}

void display_info::DisableAllBtn() {
	this->Is_RTS_Enable = FALSE;
	this->Is_Pow_Enable = FALSE;
	this->Is_Burn_Enable = FALSE;
	this->Is_Fac_Enable = FALSE;
	this->Is_Log1_Enable = FALSE;
	this->Is_Log2_Enable = FALSE;
	this->Is_Log3_Enable = FALSE;
	this->Is_Log4_Enable = FALSE;
}
