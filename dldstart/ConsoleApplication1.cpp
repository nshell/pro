﻿// ConsoleApplication1.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <stdlib.h>
#include <string.h>
#include <Windows.h>
#include <stdio.h>
#include "DldTool.h"
#include <tchar.h>
#include <iostream>
#include <atlstr.h>

#pragma comment( linker, "/subsystem:windows /entry:mainCRTStartup" )

#ifdef _DEBUG
#pragma comment(lib,"transferdll.lib")
#else
#pragma comment(lib,"transferdll.lib")
#endif

    using namespace std;
    HANDLE hPipe;
    DWORD wlen=0;
enum {
    DEFAULT_CALIBRATE_VALUE = 127,
};

void MonitorDownloadEvent_ramrun()
{
    char buf[256] = "准备发出";
    sprintf_s(buf, "%s", buf);
    while (1)
    {

        if (WriteFile(hPipe, buf, sizeof(buf), &wlen, 0) == FALSE)	//向服务器发送内容
        {
            printf("write to pipe failed!\n");
            break;

        }
        else
        {
            printf("To Master: data = %s, size = %d\n", buf, wlen);

       }
        Sleep(10);
        unsigned long notifyValue = get_notify_from_cext();
        sprintf_s(buf, "state:%d", notifyValue);
        WriteFile(hPipe, buf, sizeof(buf), &wlen, 0);
        bool bFinished = false;
        switch (notifyValue)
        {
        case ev_wm_port_open_failed:
          printf("状态：串口打开失败"); break;
        case ev_wm_port_open_succeed:
          printf("状态：串口打开成功"); break;
        case ev_wm_sync_wait:
          printf("状态：等待中");   break;
        case ev_wm_sync_failed:
          printf("状态：同步失败"); break;
        case ev_wm_sync_succeed:
          printf("状态：同步成功"); break;
        case ev_wm_run_programmer_failed:
          printf("状态：运行失败"); break;
        case ev_wm_run_programmer_succeed:
          printf("状态：运行成功"); break;
        case ev_wm_bin_encrypt_begin:
          printf("状态：加密开始"); break;
        case ev_wm_bin_encrypt_end:
          printf("状态：加密完成"); break;
        case ev_wm_update_sw_ver:
          printf("状态：更新软件版本"); break;
        case ev_wm_update_product_id:
          printf("状态：更新制造数字"); break;
        case ev_wm_burn_progress:
          printf("状态：烧写中");
          {   int iProcess = get_burn_progress();
          sprintf_s(buf, "state:%d,iProcess:%d", notifyValue, iProcess);
          WriteFile(hPipe, buf, sizeof(buf), &wlen, 0);
          }
            break;
        case ev_wm_burn_magic:
          printf("状态：烧写magic"); break;
        case ev_wm_burn_failure:
          printf("状态：烧写失败"); break;
        case ev_wm_burn_complt:
          printf("状态：烧写完成");
          bFinished = true;
            break;
        case ev_wm_burn_efuse_start:
          printf("状态：烧写efuse开始"); break;
        case ev_wm_burn_efuse_end:
          printf("状态：烧写efuse结束"); break;
        case ev_wm_factory_mode:
          printf("状态：工厂模式"); break;
        case ev_wm_block_for_audition:
          printf("状态：aud阻塞"); break;
        case ev_wm_audition_failure:
          printf("状态：aud失败"); break;
        case ev_wm_burn_audsec_success:
          printf("状态：烧写aud成功"); break;
        case ev_wm_burn_audsec_failure:
          printf("状态：烧写aud失败"); break;
        case ev_wm_chip_poweroff:
          printf("状态：芯片关机"); break;
        case ev_wm_ready_next_work:
          printf("状态：准备下一个工作"); break;
        case ev_wm_exit_valid:
          printf("状态：退出成功");
            bFinished = true;
            break;
        case ev_wm_exit_invalid:
          printf("状态：退出失败");
            bFinished = true;
            break;
        case ev_wm_factory_mode_success:
          printf("状态：工厂模式成功");
            bFinished = true;
            break;
        case ev_wm_factory_mode_progress:

          printf("状态：工厂模式进程"); break;
        case ev_wm_factory_mode_fail:
          printf("状态：工厂模式失败"); break;
        case ev_wm_factory_calib_value:
          printf("状态：工厂校准"); break;
        case ev_wm_exit_user_stop:
          printf("状态：用户停止"); break;
        case ev_wm_read_success:
          printf("状态：读取成功"); break;
        case ev_wm_read_fail:
          printf("状态：读取失败"); break;

            break;
        case ev_wm_max:
            break;
        }

        if (bFinished)
        {
            break;
        }
    }
}

const char* convtCStrToChar(CString const& strParam)
{
    CStringA cstraParam(strParam);
    rsize_t len = cstraParam.GetLength() + 1;
    char* ncharStr = new char[len];
  //  strcpy_s(ncharStr, len, strParam);
    return ncharStr;
}


int main(int argc, char* argv[])
{
    char* device;
    device = argv[10];
    CString device_str(device);
    DWORD wlen = 0;
    Sleep(10);//等待pipe的创建成功！
    if (device_str.Find(TEXT("DEVICE1")) ==0)
    {
        BOOL bRet = WaitNamedPipe(TEXT("\\\\.\\Pipe\\Evocolabs1"), NMPWAIT_WAIT_FOREVER);

        if (!bRet)
        {
            printf("connect the EvocolabsPipe failed!\n");
            system("PAUSE");
            return 0;
        }

        hPipe = CreateFile(			//管道属于一种特殊的文件
            TEXT("\\\\.\\Pipe\\Evocolabs1"),	//创建的文件名
            GENERIC_READ | GENERIC_WRITE,	//文件模式
            0,								//是否共享
            NULL,							//指向一个SECURITY_ATTRIBUTES结构的指针
            OPEN_EXISTING,					//创建参数
            FILE_ATTRIBUTE_NORMAL,			//文件属性(隐藏,只读)NORMAL为默认属性
            NULL);							//模板创建文件的句柄
    }
    else if (device_str.Find(TEXT("DEVICE2")) == 0)
    {
        BOOL bRet = WaitNamedPipe(TEXT("\\\\.\\Pipe\\Evocolabs2"), NMPWAIT_WAIT_FOREVER);

        if (!bRet)
        {
            printf("connect the EvocolabsPipe failed!\n");
            system("PAUSE");
            return 0;
        }

        hPipe = CreateFile(			//管道属于一种特殊的文件
            TEXT("\\\\.\\Pipe\\Evocolabs2"),	//创建的文件名
            GENERIC_READ | GENERIC_WRITE,	//文件模式
            0,								//是否共享
            NULL,							//指向一个SECURITY_ATTRIBUTES结构的指针
            OPEN_EXISTING,					//创建参数
            FILE_ATTRIBUTE_NORMAL,			//文件属性(隐藏,只读)NORMAL为默认属性
            NULL);							//模板创建文件的句柄
    }
    else if (device_str.Find(TEXT("DEVICE3")) == 0)
    {
        BOOL bRet = WaitNamedPipe(TEXT("\\\\.\\Pipe\\Evocolabs3"), NMPWAIT_WAIT_FOREVER);

        if (!bRet)
        {
            printf("connect the EvocolabsPipe failed!\n");
            system("PAUSE");
            return 0;
        }

        hPipe = CreateFile(			//管道属于一种特殊的文件
            TEXT("\\\\.\\Pipe\\Evocolabs3"),	//创建的文件名
            GENERIC_READ | GENERIC_WRITE,	//文件模式
            0,								//是否共享
            NULL,							//指向一个SECURITY_ATTRIBUTES结构的指针
            OPEN_EXISTING,					//创建参数
            FILE_ATTRIBUTE_NORMAL,			//文件属性(隐藏,只读)NORMAL为默认属性
            NULL);							//模板创建文件的句柄
    }
    else if (device_str.Find(TEXT("DEVICE4")) == 0)
    {
        BOOL bRet = WaitNamedPipe(TEXT("\\\\.\\Pipe\\Evocolabs4"), NMPWAIT_WAIT_FOREVER);

        if (!bRet)
        {
            printf("connect the EvocolabsPipe failed!\n");
            system("PAUSE");
            return 0;
        }

        hPipe = CreateFile(			//管道属于一种特殊的文件
            TEXT("\\\\.\\Pipe\\Evocolabs4"),	//创建的文件名
            GENERIC_READ | GENERIC_WRITE,	//文件模式
            0,								//是否共享
            NULL,							//指向一个SECURITY_ATTRIBUTES结构的指针
            OPEN_EXISTING,					//创建参数
            FILE_ATTRIBUTE_NORMAL,			//文件属性(隐藏,只读)NORMAL为默认属性
            NULL);							//模板创建文件的句柄
    }
    else
    {
        printf("设备不匹配");
        getchar();
        return 0;

    }
    if(INVALID_HANDLE_VALUE == hPipe)
    {
       printf("open the exit pipe failed!\n");
    }
    else
    {
        printf("参数：%s\n %s\n %s\n %s\n %s\n %s\n %s\n", argv[0], argv[1], argv[2], argv[3], argv[4], argv[5], argv[10]);
       
            if (argc == 11)
            {
                char* COM = "";
                char* path_app_bin = "";
                char* path_factory_bin = "";
                char* path_programmer_bin = "";
                char* path_ota_bin = "";
                char* btAddr = new char[100];
                char* bleAddr = new char[100];
                char* dangleAddr = new char[100];
                char* bt_name = new char[255];
                char* ble_name = new char[255];
                char* default_parameter = "dldtool.exe";
                char* ppath_programmer_bin = new char[100];
                char* ppath_ota_bin = new char[100];
                char* ppath_app_bin = new char[100];
                char* ppath_factory_bin = new char[100];
                char* pCOM = new char[100];
                const char* pPara[6];


                COM = argv[0];
                path_app_bin = argv[1];
                path_factory_bin = argv[2];
                path_programmer_bin = argv[3];
                path_ota_bin = argv[4];
                btAddr = argv[5];
                bleAddr = argv[6];
                dangleAddr = argv[7];
                bt_name = argv[8];
                ble_name = argv[9];

                char bt[6], ble[6], dangle[6];
                unsigned __int64  STRTONUM_TEMP_BT = 0;
                unsigned __int64  STRTONUM_TEMP_BLE = 0;
                unsigned __int64  STRTONUM_TEMP_DANGLE = 0;
                STRTONUM_TEMP_BT = _strtoui64(btAddr, NULL, 16);
                STRTONUM_TEMP_BLE = _strtoui64(bleAddr, NULL, 16);
                STRTONUM_TEMP_DANGLE = _strtoui64(dangleAddr, NULL, 16);
                for (int i = 0; i < 6; i++)
                {
                    bt[i] = STRTONUM_TEMP_BT & 0XFF;
                    STRTONUM_TEMP_BT = STRTONUM_TEMP_BT >> 8;
                    ble[i] = STRTONUM_TEMP_BLE & 0XFF;
                    STRTONUM_TEMP_BLE = STRTONUM_TEMP_BLE >> 8;
                    dangle[i] = STRTONUM_TEMP_DANGLE & 0XFF;
                    STRTONUM_TEMP_DANGLE = STRTONUM_TEMP_DANGLE >> 8;
                }



                handle_buildinfo_to_extend(path_app_bin);

                userdata_sector_gen(path_factory_bin);




                sector_gen(path_factory_bin,
                    bt, //BT ADDR:
                    ble, //BLE ADDR
                    dangle, //DANGLE ADDR
                    bt_name,      //BT NAME
                    ble_name,     //BLE NAME
                    DEFAULT_CALIBRATE_VALUE);   //CALIBRATE 


                CString comport(COM);
                comport = TEXT("\\\\.\\") + comport;
                int fpos, bpos;
                fpos = comport.Find('M');
                CString temp;
                temp = comport.Mid(fpos + 1, 2);
                temp = TEXT("-C") + temp;
                CT2A ascii0(temp);
                pCOM = ascii0.m_psz;

                temp = CString(path_factory_bin);
                temp = TEXT("-f") + temp;
                CT2A ascii1(temp);
                ppath_factory_bin = ascii1.m_psz;

                temp = CString(path_app_bin);
                temp = TEXT("-b") + temp;
                CT2A ascii2(temp);
                ppath_app_bin = ascii2.m_psz;

                temp = CString(path_ota_bin);
                temp = TEXT("-b") + temp;
                CT2A ascii3(temp);
                ppath_ota_bin = ascii3.m_psz;



                temp = CString(path_programmer_bin);
                temp = TEXT("-r") + temp;
                CT2A ascii4(temp);
                ppath_programmer_bin = ascii4.m_psz;




                pPara[0] = default_parameter;   //default parameter  
                pPara[1] = pCOM;
                pPara[2] = ppath_programmer_bin;


                pPara[3] = ppath_factory_bin;//加隐藏，最好能删除


                pPara[4] = ppath_app_bin;
                pPara[5] = ppath_ota_bin;

                printf("参数：%s\n %s\n %s\n %s\n %s\n %s\n %s\n", pPara[0], pPara[1], pPara[2], pPara[3], pPara[4], pPara[5],argv[10]);
             //   getchar();
                HANDLE hPort = CreateFile(comport, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
                if (hPort == INVALID_HANDLE_VALUE)
                {
                    DWORD dwError = GetLastError();
                    if (dwError == ERROR_ACCESS_DENIED)
                    {
                       printf("状态：串口占用");
                       getchar();
                    }
                }
                else
                {
                    //The port was opened successfully
             
                    char buf[256] = "state:12580";
                    //DCB dcb;
                    //GetCommState(hPort, &dcb);
                    //dcb.BaudRate = 9600;//设置波动率9600
                    //dcb.ByteSize = 8;//数据长度8位
                    //dcb.Parity = NOPARITY;//无校验位
                    //dcb.StopBits = ONESTOPBIT;//2停止位
                   // SetCommState(hPort, &dcb);// 设置COM口的设备控制块
                   // dcb.fRtsControl = RTS_CONTROL_ENABLE;
                   // dcb.fDtrControl = DTR_CONTROL_ENABLE;
                    //SetCommState(hPort, &dcb);// 设置COM口的设备控制块
                    printf("burn preparing");
                    WriteFile(hPipe, buf, sizeof(buf), &wlen, 0);
                   // Sleep(1000);
                    CloseHandle(hPort);
                   // Sleep(300);
                    dldstart(6, pPara);
                    MonitorDownloadEvent_ramrun();
        
                    dldstop();

                }

                
            }
            
            if (argc != 11)
            {
                while (true)
                {
                    char buf[256] = "";
                    sprintf_s(buf, "%s%d", buf, rand() % 1000);
                    if (WriteFile(hPipe, buf, sizeof(buf), &wlen, 0) == FALSE)	//向服务器发送内容
                    {
                        printf("write to pipe failed!\n");
                        break;
                    }
                    else
                    {
                        printf("To Server: data = %s, size = %d\n", buf, wlen);
                        printf("参数数量：%d\n", argc);
                        printf("参数：%s\n %s\n %s\n %s\n %s\n %s\n %s\n %s\n %s\n %s\n", argv[0], argv[1], argv[2], argv[3], argv[4], argv[5],argv[6],argv[7], argv[8], argv[9]);

                    }
                    Sleep(1000);

                }
          }
           
            Sleep(1000);
        
        CloseHandle(hPipe);//关闭管道
    }

   




 

    return EXIT_SUCCESS;
}

